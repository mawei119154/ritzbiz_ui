import Vue from "vue";
import VueRouter from "vue-router";
import Layout from "@/views/layout/Layout";
// const routerPush = VueRouter.prototype.push;
// VueRouter.prototype.push = function push(location) {
//   return routerPush.call(this, location).catch(error => error);
// };

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "",
      component: Layout,
      children: [
        {
          path: "/",
          name: "logo",
          component: () =>
            import(/*webpackChunkName: "logo" */ "../views/logo/index")
        },
        {
          path: "/trade",
          name: "trade",
          component: () => import("../views/trade/index")
        },
        {
          path: "/merchant",
          name: "merchant",
          component: () => import("../views/merchant/index")
        },
        {
          path: "/tradeOrderDetail",
          name: "tradeOrderDetail",
          component: () => import("../views/trade/orderDetail")
        },
        {
          path: "/tradeOrderList",
          name: "tradeOrderList",
          component: () => import("../views/trade/orderList")
        },
        {
          path: "/tradeAdsDetail",
          name: "tradeAdsDetail",
          component: () => import("../views/trade/adsDetail")
        },
        {
          path: "/tradeAdsList",
          name: "tradeAdsList",
          component: () => import("../views/trade/adsList")
        },
        {
          path: "/order",
          name: "order",
          component: () => import("../views/order/index")
        },
        {
          path: "/orderDetail",
          name: "orderDetail",
          component: () => import("../views/order/detailOrder")
        },
        {
          path: "/assets",
          name: "assets",
          component: () => import("../views/assets/index")
        },
        {
          path: "/record",
          name: "record",
          component: () => import("../views/assets/record")
        },
        {
          path: "/address",
          name: "address",
          component: () => import("../views/assets/address")
        },
        {
          path: "/profile",
          name: "profile",
          component: () => import("../views/profile/index")
        },
        {
          path: "/authentication",
          name: "authentication",
          component: () => import("../views/authentication")
        },
        {
          path: "/platform",
          name: "platform",
          component: () => import("../views/authentication/platform")
        },
        {
          path: "/login-pass",
          name: "loginPass",
          component: () => import("../views/loginPass/index")
        },
        {
          path: "/fund-pass",
          name: "fundPass",
          component: () => import("../views/fundPass/index")
        },
        {
          path: "/bind",
          component: () => import("../views/bind/index"),
          children: [
            {
              path: "",
              component: () => import("../views/bind/phone")
            },
            {
              path: "email",
              component: () => import("../views/bind/email")
            },
            {
              path: "phone",
              component: () => import("../views/bind/phone")
            },
            {
              path: "google",
              component: () => import("../views/bind/google")
            }
          ]
        }
      ]
    },
    {
      path: "/unlock",
      component: () =>
        import(/*webpackChunkName: "unlock" */ "@/views/login/Index"),
      // hidden: true,
      children: [
        {
          path: "",
          redirect: "login"
          // component: () => import('@/views/login/Login'),
        },
        {
          path: "login",
          name: "login",
          component: () =>
            import(/* webpackChunkName: "login" */ "@/views/login/Login"),
          meta: { title: "登陆", icon: "login" }
        },
        {
          path: "register",
          name: "register",
          component: () => import("@/views/login/Register"),
          meta: { title: "注册", icon: "register" }
        },
        {
          path: "reset",
          name: "reset",
          component: () => import("@/views/login/ResetPass"),
          meta: { title: "忘记密码", icon: "reset" }
        },
        {
          path: "*",
          // redirect: "/",
          redirect: "login",
          meta: {}
        }
      ]
    },
    {
      path: "*",
      redirect: "/",
      meta: {}
    }
  ]
});

export default router;
