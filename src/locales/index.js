import Vue from "vue";
import VueI18n from "vue-i18n";
Vue.use(VueI18n);

let lang =
  (navigator.languages && navigator.languages[0]) ||
  navigator.language ||
  navigator.browserLanguage.toLowerCase();
lang = lang.split("-")[0];
console.log(lang, "语言", navigator.languages);

let zh = Object.assign(require("./zh.json"));
let en = Object.assign(require("./en.json"));
const LOCALE_KEY = "localeLanguage";
const DEFAULT_LANG = "zh";

if (window.localStorage.getItem(LOCALE_KEY)) {
  lang = window.localStorage.getItem(LOCALE_KEY);
} else {
  if (lang !== DEFAULT_LANG) {
    lang = "en";
    window.localStorage.setItem(LOCALE_KEY, lang);
  }
}

const locales = {
  zh: zh,
  en: en
};

const i18n = new VueI18n({
  locale: lang,
  messages: locales
});
Vue.config.lang = lang;
i18n.locale = lang;
window.i18n = i18n;
export default i18n;
