/* eslint-disable no-undef */
// src/service-worker.js
// importScripts("https://g.alicdn.com/kg/workbox/3.3.0/workbox-sw.js");

if (workbox) {
  console.log("have workbox");
  workbox.setConfig({
    debug: true
  });

  workbox.core.skipWaiting();
  workbox.core.clientsClaim();

  // // 设置相应缓存的名字的前缀和后缀
  workbox.core.setCacheNameDetails({
    prefix: "jm",
    suffix: "v1.5",
    precache: "precache",
    runtime: "runtime"
  });
  // 让我们的service worker尽快的得到更新和获取页面的控制权

  // workbox.core.setLogLevel(workbox.core.LOG_LEVELS.debug);
  /*
   * vue-cli3.0通过workbox-webpack-plagin 来实现相关功能，我们需要加入
   * 以下语句来获取预缓存列表和预缓存他们，也就是打包项目后生产的html，js，css等* 静态文件
   */

  // workbox.precaching.precacheAndRoute(self.__precacheManifest || []);
  workbox.precaching.precacheAndRoute(self.__precacheManifest);
  workbox.routing.registerNavigationRoute("/index.html", options => {
    console.log(options);
  });

  workbox.routing.registerRoute(
    /^https:\/\/res.wx.qq.com\/mpres\/htmledition\/images\/icon\/emotion\/.*/,
    new workbox.strategies.CacheFirst({
      cacheName: "qq-emotion",
      plugins: [
        new workbox.cacheableResponse.Plugin({
          statuses: [0, 200]
        }),
        new workbox.expiration.Plugin({
          maxAgeSeconds: 60 * 60 * 24 * 365,
          maxEntries: 30
        })
      ]
    })
  );
  workbox.routing.registerRoute(
    /^https:\/\/s2.ax1x.com\/.*/,
    new workbox.strategies.CacheFirst({
      cacheName: "coutry-icon",
      plugins: [
        new workbox.cacheableResponse.Plugin({
          statuses: [0, 200]
        }),
        new workbox.expiration.Plugin({
          maxAgeSeconds: 60 * 60 * 24 * 365,
          maxEntries: 30
        })
      ]
    })
  );

  // 对我们请求的数据进行缓存，这里采用 networkFirst 策略
  workbox.routing.registerRoute(
    /.*.css$/,
    new workbox.strategies.StaleWhileRevalidate({
      cacheName: "css-cache"
    })
  );
  workbox.routing.registerRoute(
    /.*.json$/,
    new workbox.strategies.StaleWhileRevalidate({
      cacheName: "json-cache"
    })
  );
  workbox.routing.registerRoute(
    /.*\/img\/.*/,
    new workbox.strategies.CacheFirst({
      cacheName: "image-runtime-cache"
    })
  );
  workbox.routing.registerRoute(
    /.*\/data:image\/.*/,
    new workbox.strategies.CacheFirst({
      cacheName: "image-runtime-base64-cache"
    })
  );
  workbox.routing.registerRoute(
    /.*.js$/,
    new workbox.strategies.StaleWhileRevalidate({
      cacheName: "js-cache"
    })
  );
  workbox.routing.registerRoute(
    /.*\/doc\/.*/,
    new workbox.strategies.StaleWhileRevalidate({
      cacheName: "doc-cache"
    })
  );
  workbox.routing.registerRoute(
    /.*\/fonts\/.*/,
    new workbox.strategies.StaleWhileRevalidate({
      cacheName: "fonts-cache"
    })
  );
  workbox.routing.registerRoute(
    /.*.ttf$/,
    new workbox.strategies.StaleWhileRevalidate({
      cacheName: "fonts-cache"
    })
  );
  workbox.routing.registerRoute(
    /.*.eot$/,
    new workbox.strategies.StaleWhileRevalidate({
      cacheName: "fonts-cache"
    })
  );

  /**
   * api
   */
  // BackgroundSyncPlugin 将保存对此路由的任何失败的“ POST”请求，并在用户重新联机后重播！
  const bgSyncPlugin = new workbox.backgroundSync.Plugin("myQueueName", {
    maxRetentionTime: 24 * 60 // Retry for max of 24 Hours (specified in minutes)
  });
  workbox.routing.registerRoute(
    /.*\/coin\/coins/,
    new workbox.strategies.NetworkOnly({
      cacheName: "api-cache",
      plugins: [bgSyncPlugin]
    }),
    "POST"
  );
  workbox.routing.registerRoute(
    /.*\/market\/queryCountryList$/,
    new workbox.strategies.NetworkOnly({
      cacheName: "api-cache",
      plugins: [bgSyncPlugin]
    }),
    "POST"
  );
  workbox.routing.registerRoute(
    /.*\/market\/queryGiftCard$/,
    new workbox.strategies.NetworkOnly({
      cacheName: "api-cache",
      plugins: [bgSyncPlugin]
    }),
    "POST"
  );
  workbox.routing.registerRoute(
    //根据国家获取法币
    /.*\/market\/queryOtcMarketCurrency.*/,
    new workbox.strategies.NetworkOnly({
      cacheName: "api-cache",
      plugins: [bgSyncPlugin]
    }),
    "POST"
  );
  workbox.routing.registerRoute(
    /.*\/receiptWay\/list/,
    new workbox.strategies.NetworkOnly({
      cacheName: "api-cache",
      plugins: [bgSyncPlugin]
    }),
    "POST"
  );
  workbox.routing.registerRoute(
    /.*\/user\/getLoginLog/,
    new workbox.strategies.NetworkOnly({
      cacheName: "api-cache",
      plugins: [bgSyncPlugin]
    }),
    "POST"
  );
  workbox.routing.registerRoute(
    /.*\/user\/getOwnInfo/,
    new workbox.strategies.NetworkOnly({
      cacheName: "api-cache",
      plugins: [bgSyncPlugin]
    }),
    "POST"
  );
  workbox.routing.registerRoute(
    /.*\/assets\/getAssets/,
    new workbox.strategies.NetworkOnly({
      cacheName: "api-cache",
      plugins: [bgSyncPlugin]
    }),
    "POST"
  );
  workbox.routing.registerRoute(
    /.*\/assets\/getAssetsConvertUsdtAmount/,
    new workbox.strategies.NetworkOnly({
      cacheName: "api-cache",
      plugins: [bgSyncPlugin]
    }),
    "POST"
  );
} else {
  console.log("no workbox");
}
