import router from "./router";
// import {resetRouter} from './router'

// const whiteList = ["/login", "/download"]; // 不重定向白名单
// import store from './store'
// import { menusToRoutes, resetTokenAndClearUser } from './utils'
// // import axios from 'axios'

// router.beforeEach(async (to, from, next) => {
//     if (localStorage.getItem('token')) {
//         console.log(1)
//         if (to.path.indexOf('/login')>=0) {
//             next({path: '/'})
//         } else {
//                 try {
//                     // 这里可以用 await 配合请求后台数据来生成路由
//                     // const data = await axios.get('xxx')
//                     // const routes = menusToRoutes(data)
//                     // const routes = menusToRoutes()
//                     // // 动态添加路由
//                     // router.addRoutes(routes)
//                     console.log(from.path)
//                     next({path: to.path || '/'})
//                 } catch (error) {
//                     resetTokenAndClearUser()
//                     next(`/login?redirect=${to.path}`)
//                 }
//         }
//     } else {
//         console.log(2)
//         if (to.path.indexOf('/login')>=0) {
//             next()
//         } else {
//             // next(`/login?redirect=${to.path}`)
//             next(`/login?redirect=${to.path}`)
//         }
//     }
// })

// router.afterEach(() => {

// })

router.beforeEach(async (to, from, next) => {
  // console.log(1,from.path,to.path)
  // console.log(router,localStorage.getItem('token'))
  // 以下是正常使用的时候需要放开的
  if (localStorage.getItem("token")) {
    // resetRouter()
    if (to.path.indexOf("/unlock") >= 0) {
      next("/");
    } else {
      next();
      // next({path: to.path || '/'})
    }
  } else {
    // if (whiteList.indexOf(to.path) >=0) {
    if (to.path.indexOf("/unlock") >= 0 || to.path == "/") {
      next();
    } else {
      next(`/unlock`);
    }
  }
  // next();
  // next({path: to.path || '/'})
});
