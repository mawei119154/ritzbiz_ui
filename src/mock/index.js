import Mock from "mockjs";
Mock.setup({
  timeout: 2000
});

// 修复在使用 MockJS 情况下，设置 withCredentials = true，且未被拦截的跨域请求丢失 Cookies 的问题
// https://github.com/nuysoft/Mock/issues/300
Mock.XHR.prototype.proxy_send = Mock.XHR.prototype.send;
Mock.XHR.prototype.send = function() {
  if (this.custom.xhr) {
    this.custom.xhr.withCredentials = this.withCredentials || false;
  }
  this.proxy_send(...arguments);
};
let Random = Mock.Random;

if (process.env.NODE_ENV !== "production") {
  // 登录相关
  Mock.mock(/manage\/login/, "post", {
    code: 200,
    data: {
      "accountIdentityCheckStatus|1": [-1, 0, 1, 2],
      logo: Random.image("200x100", "#50B347", "#FFF"),
      phone: /^1[385][1-9]\d{8}/,
      username: Random.cname()
    },
    msg: "成功"
  });
  Mock.mock(/manage\/logout/, "get", {
    code: 200,
    data: null,
    msg: "成功"
  });
  Mock.mock(/manage\/registered/, "post", {});
  Mock.mock(/manage\/country\/list/, "get", {
    code: 200,
    msg: "成功",
    data: [
      {
        id: 4,
        countryName: "中国香港",
        countryCode: "HK",
        currencyCode: "HKD",
        logo: "https://s2.ax1x.com/2019/05/14/EIAPqs.png"
      },
      {
        id: 3,
        countryName: "新加坡",
        countryCode: "SG",
        currencyCode: "SGD",
        logo: "https://s2.ax1x.com/2019/05/14/EIACrj.png"
      },
      {
        id: 2,
        countryName: "美国",
        countryCode: "USA",
        currencyCode: "USD",
        logo: "https://s2.ax1x.com/2019/05/14/EIA9MQ.png"
      },
      {
        id: 1,
        countryName: "中国",
        countryCode: "CN",
        currencyCode: "RMB",
        logo: "https://s2.ax1x.com/2019/05/14/EIAFZn.png"
      }
    ]
  });
  Mock.mock(/manage\/dict\/list\/%7BbizType%7D\?bizType=1/, "get", {
    code: 200,
    msg: "成功",
    data: [
      {
        id: 1,
        type: 1,
        name: "工资",
        remark: null
      },
      {
        id: 2,
        type: 1,
        name: "储蓄",
        remark: null
      },
      {
        id: 3,
        type: 1,
        name: "经营收入",
        remark: null
      },
      {
        id: 4,
        type: 1,
        name: "投资收益",
        remark: null
      },
      {
        id: 5,
        type: 1,
        name: "贷款",
        remark: null
      },
      {
        id: 6,
        type: 1,
        name: "兼职",
        remark: null
      }
    ]
  });
  Mock.mock(/manage\/dict\/list\/%7BbizType%7D\?bizType=2/, "get", {
    code: 200,
    msg: "成功",
    data: [
      { id: 7, type: 2, name: "赡家款", remark: null },
      { id: 8, type: 2, name: "职工报酬", remark: null },
      { id: 9, type: 2, name: "捐赠", remark: null },
      { id: 10, type: 2, name: "专有权利使用和特许收入", remark: null },
      {
        id: 11,
        type: 2,
        name: "法律、会计咨询和公共关系服务收入",
        remark: null
      },
      { id: 12, type: 2, name: "其他用途", remark: null }
    ]
  });
  Mock.mock(/manage\/dict\/list\/%7BbizType%7D\?bizType=3/, "get", {
    code: 200,
    msg: "成功",
    data: [
      {
        id: 13,
        type: 3,
        name: "自己",
        remark: null
      },
      {
        id: 14,
        type: 3,
        name: "家人",
        remark: null
      },
      {
        id: 15,
        type: 3,
        name: "朋友",
        remark: null
      },
      {
        id: 16,
        type: 3,
        name: "亲戚",
        remark: null
      },
      {
        id: 17,
        type: 3,
        name: "工作伙伴",
        remark: null
      },
      {
        id: 18,
        type: 3,
        name: "其他",
        remark: null
      }
    ]
  });
  Mock.mock(/manage\/payee\/list/, "get", {
    code: 200,
    "data|1-10": [
      {
        "id|+1": 1,
        surname: "@cfirst",
        name: "@clast",
        phone: /^1[385][1-9]\d{8}/,
        "bankId|1": ["中国人民银行", "中国建设银行", "广州发展银行"],
        bankLogo: Random.image("20x20"),
        bankCardNumber: /^[1-9]\d{9,29}$/,
        "bizDictId|13-18": 1,
        identityCode: /(^\d{8}(0\d|10|11|12)([0-2]\d|30|31)\d{3}$)|(^\d{6}(18|19|20)\d{2}(0\d|10|11|12)([0-2]\d|30|31)\d{3}(\d|X|x)$)/,
        bankBindPhone: /^1[385][1-9]\d{8}/,
        "identityIntactTag|1-2": true
      }
    ],
    msg: "okk"
  });
  Mock.mock(/manage\/payee\/delete/, "post", {
    code: 200,
    data: null,
    msg: "删除成功"
  });
  Mock.mock(/manage\/payee\/update/, "post", {
    code: 200,
    data: null,
    msg: "删除成功"
  });
  Mock.mock(/manage\/payee\/save/, "post", {
    code: 200,
    data: null,
    msg: "删除成功"
  });
  Mock.mock(/manage\/coupon\/pagelist/, "post", () => {
    return Mock.mock({
      code: 0,
      data: {
        "list|10": [
          {
            showName: "@csentence(10)",
            addRate: "@float(0.001, 0.01, 3, 4)", //汇率提升，如0.005（按rmb金额计算，amount+amount*addRate）
            "amount|1-80": 50, //优惠券金额，满减券和现金券使用是字段金额
            endTime: "@date",
            explain: "@csentence(3, 5)",
            "id|+1": 1,
            "fullReductionAmount|50-200": 100, //满减金额，如1000-50，从amount取金额
            name: "@csentence(8)",
            startTime: "@date",
            "status|1": [0, 1, 2],
            statusText: "@csentence(3)",
            "type|1": [0, 1, 2, 3],
            typeText: "@csentence(4)"
          }
        ],
        "total|1": [1000, 400, 700, 100]
      },
      msg: "string"
    });
  });
  Mock.mock(/manage\/coupon\/list/, "post", () => {
    return Mock.mock({
      code: 0,
      "data|1-10": [
        {
          showName: "@csentence(10)",
          addRate: "@float(0.001, 0.01, 3, 4)", //汇率提升，如0.005（按rmb金额计算，amount+amount*addRate）
          "amount|1-80": 50, //优惠券金额，满减券和现金券使用是字段金额
          endTime: "@date",
          explain: "@csentence(3, 5)", //优惠券说明
          "id|+1": 1, //优惠券编号
          "fullReductionAmount|50-200": 100, //满减金额，如1000-50，从amount取金额
          name: "@csentence(8)", //优惠券名称
          startTime: "@date",
          "status|1": 0, //优惠券状态：0可使用，1已使用，已过期
          statusText: "@csentence(3)", //优惠券状态文本
          "type|1": [0, 1, 2, 3], // 优惠券类型：0汇率提升券，1满减，2免手续费，3现金券
          typeText: "@csentence(4)" //优惠券类型文本
        }
      ],
      msg: "string"
    });
  });
  Mock.mock(/manage\/bankcard\/list/, "get", {
    code: 200,
    data: [
      {
        id: 3,
        name: " 中国银行",
        code: "BOC",
        logo: "C:\\home\\bank_logo\\BOC.png",
        bgLogo: null
      },
      {
        id: 4,
        name: " 中国建设银行",
        code: "CCB",
        logo: "C:\\home\\bank_logo\\CCB.png",
        bgLogo: null
      },
      {
        id: 5,
        name: " 平安银行",
        code: "SPABANK",
        logo: "C:\\home\\bank_logo\\SPABANK.png",
        bgLogo: null
      },
      {
        id: 6,
        name: " 招商银行",
        code: "CMB",
        logo: "C:\\home\\bank_logo\\CMB.png",
        bgLogo: null
      },
      {
        id: 7,
        name: " 深圳农村商业银行",
        code: "SRCB",
        logo: "C:\\home\\bank_logo\\SRCB.png",
        bgLogo: null
      },
      {
        id: 8,
        name: " 广西北部湾银行",
        code: "BGB",
        logo: "C:\\home\\bank_logo\\BGB.png",
        bgLogo: null
      },
      {
        id: 9,
        name: " 上海农村商业银行",
        code: "SHRCB",
        logo: "C:\\home\\bank_logo\\SHRCB.png",
        bgLogo: null
      },
      {
        id: 10,
        name: " 北京银行",
        code: "BJBANK",
        logo: "C:\\home\\bank_logo\\BJBANK.png",
        bgLogo: null
      },
      {
        id: 11,
        name: " 威海市商业银行",
        code: "WHCCB",
        logo: "C:\\home\\bank_logo\\WHCCB.png",
        bgLogo: null
      },
      {
        id: 12,
        name: " 周口银行",
        code: "BOZK",
        logo: "C:\\home\\bank_logo\\BOZK.png",
        bgLogo: null
      },
      {
        id: 13,
        name: " 库尔勒市商业银行",
        code: "KORLABANK",
        logo: "C:\\home\\bank_logo\\KORLABANK.png",
        bgLogo: null
      },
      {
        id: 14,
        name: " 顺德农商银行",
        code: "SDEB",
        logo: "C:\\home\\bank_logo\\SDEB.png",
        bgLogo: null
      },
      {
        id: 15,
        name: " 湖北省农村信用社",
        code: "HURCB",
        logo: "C:\\home\\bank_logo\\HURCB.png",
        bgLogo: null
      },
      {
        id: 16,
        name: " 无锡农村商业银行",
        code: "WRCB",
        logo: "C:\\home\\bank_logo\\WRCB.png",
        bgLogo: null
      },
      {
        id: 17,
        name: " 朝阳银行",
        code: "BOCY",
        logo: "C:\\home\\bank_logo\\BOCY.png",
        bgLogo: null
      },
      {
        id: 18,
        name: " 浙商银行",
        code: "CZBANK",
        logo: "C:\\home\\bank_logo\\CZBANK.png",
        bgLogo: null
      },
      {
        id: 19,
        name: " 邯郸银行",
        code: "HDBANK",
        logo: "C:\\home\\bank_logo\\HDBANK.png",
        bgLogo: null
      },
      {
        id: 20,
        name: " 东莞银行",
        code: "BOD",
        logo: "C:\\home\\bank_logo\\BOD.png",
        bgLogo: null
      },
      {
        id: 21,
        name: " 遵义市商业银行",
        code: "ZYCBANK",
        logo: "C:\\home\\bank_logo\\ZYCBANK.png",
        bgLogo: null
      },
      {
        id: 22,
        name: " 绍兴银行",
        code: "SXCB",
        logo: "C:\\home\\bank_logo\\SXCB.png",
        bgLogo: null
      },
      {
        id: 23,
        name: " 贵州省农村信用社",
        code: "GZRCU",
        logo: "C:\\home\\bank_logo\\GZRCU.png",
        bgLogo: null
      },
      {
        id: 24,
        name: " 张家口市商业银行",
        code: "ZJKCCB",
        logo: "C:\\home\\bank_logo\\ZJKCCB.png",
        bgLogo: null
      },
      {
        id: 25,
        name: " 锦州银行",
        code: "BOJZ",
        logo: "C:\\home\\bank_logo\\BOJZ.png",
        bgLogo: null
      },
      {
        id: 26,
        name: " 平顶山银行",
        code: "BOP",
        logo: "C:\\home\\bank_logo\\BOP.png",
        bgLogo: null
      },
      {
        id: 27,
        name: " 汉口银行",
        code: "HKB",
        logo: "C:\\home\\bank_logo\\HKB.png",
        bgLogo: null
      },
      {
        id: 28,
        name: " 上海浦东发展银行",
        code: "SPDB",
        logo: "C:\\home\\bank_logo\\SPDB.png",
        bgLogo: null
      },
      {
        id: 29,
        name: " 宁夏黄河农村商业银行",
        code: "NXRCU",
        logo: "C:\\home\\bank_logo\\NXRCU.png",
        bgLogo: null
      },
      {
        id: 30,
        name: " 广东南粤银行",
        code: "NYNB",
        logo: "C:\\home\\bank_logo\\NYNB.png",
        bgLogo: null
      },
      {
        id: 31,
        name: " 广州农商银行",
        code: "GRCB",
        logo: "C:\\home\\bank_logo\\GRCB.png",
        bgLogo: null
      },
      {
        id: 32,
        name: " 苏州银行",
        code: "BOSZ",
        logo: "C:\\home\\bank_logo\\BOSZ.png",
        bgLogo: null
      },
      {
        id: 33,
        name: " 杭州银行",
        code: "HZCB",
        logo: "C:\\home\\bank_logo\\HZCB.png",
        bgLogo: null
      },
      {
        id: 34,
        name: " 衡水银行",
        code: "HSBK",
        logo: "C:\\home\\bank_logo\\HSBK.png",
        bgLogo: null
      },
      {
        id: 35,
        name: " 湖北银行",
        code: "HBC",
        logo: "C:\\home\\bank_logo\\HBC.png",
        bgLogo: null
      },
      {
        id: 36,
        name: " 嘉兴银行",
        code: "JXBANK",
        logo: "C:\\home\\bank_logo\\JXBANK.png",
        bgLogo: null
      },
      {
        id: 37,
        name: " 华融湘江银行",
        code: "HRXJB",
        logo: "C:\\home\\bank_logo\\HRXJB.png",
        bgLogo: null
      },
      {
        id: 38,
        name: " 丹东银行",
        code: "BODD",
        logo: "C:\\home\\bank_logo\\BODD.png",
        bgLogo: null
      },
      {
        id: 39,
        name: " 安阳银行",
        code: "AYCB",
        logo: "C:\\home\\bank_logo\\AYCB.png",
        bgLogo: null
      },
      {
        id: 40,
        name: " 恒丰银行",
        code: "EGBANK",
        logo: "C:\\home\\bank_logo\\EGBANK.png",
        bgLogo: null
      },
      {
        id: 41,
        name: " 国家开发银行",
        code: "CDB",
        logo: "C:\\home\\bank_logo\\CDB.png",
        bgLogo: null
      },
      {
        id: 42,
        name: " 江苏太仓农村商业银行",
        code: "TCRCB",
        logo: "C:\\home\\bank_logo\\TCRCB.png",
        bgLogo: null
      },
      {
        id: 43,
        name: " 南京银行",
        code: "NJCB",
        logo: "C:\\home\\bank_logo\\NJCB.png",
        bgLogo: null
      },
      {
        id: 44,
        name: " 郑州银行",
        code: "ZZBANK",
        logo: "C:\\home\\bank_logo\\ZZBANK.png",
        bgLogo: null
      },
      {
        id: 45,
        name: " 德阳商业银行",
        code: "DYCB",
        logo: "C:\\home\\bank_logo\\DYCB.png",
        bgLogo: null
      },
      {
        id: 46,
        name: " 宜宾市商业银行",
        code: "YBCCB",
        logo: "C:\\home\\bank_logo\\YBCCB.png",
        bgLogo: null
      },
      {
        id: 47,
        name: " 四川省农村信用",
        code: "SCRCU",
        logo: "C:\\home\\bank_logo\\SCRCU.png",
        bgLogo: null
      },
      {
        id: 48,
        name: " 昆仑银行",
        code: "KLB",
        logo: "C:\\home\\bank_logo\\KLB.png",
        bgLogo: null
      },
      {
        id: 49,
        name: " 莱商银行",
        code: "LSBANK",
        logo: "C:\\home\\bank_logo\\LSBANK.png",
        bgLogo: null
      },
      {
        id: 50,
        name: " 尧都农商行",
        code: "YDRCB",
        logo: "C:\\home\\bank_logo\\YDRCB.png",
        bgLogo: null
      },
      {
        id: 51,
        name: " 重庆三峡银行",
        code: "CCQTGB",
        logo: "C:\\home\\bank_logo\\CCQTGB.png",
        bgLogo: null
      },
      {
        id: 52,
        name: " 富滇银行",
        code: "FDB",
        logo: "C:\\home\\bank_logo\\FDB.png",
        bgLogo: null
      },
      {
        id: 53,
        name: " 江苏省农村信用联合社",
        code: "JSRCU",
        logo: "C:\\home\\bank_logo\\JSRCU.png",
        bgLogo: null
      },
      {
        id: 54,
        name: " 济宁银行",
        code: "JNBANK",
        logo: "C:\\home\\bank_logo\\JNBANK.png",
        bgLogo: null
      },
      {
        id: 55,
        name: " 晋城银行JCBANK",
        code: "JINCHB",
        logo: "C:\\home\\bank_logo\\JINCHB.png",
        bgLogo: null
      },
      {
        id: 56,
        name: " 阜新银行",
        code: "FXCB",
        logo: "C:\\home\\bank_logo\\FXCB.png",
        bgLogo: null
      },
      {
        id: 57,
        name: " 武汉农村商业银行",
        code: "WHRCB",
        logo: "C:\\home\\bank_logo\\WHRCB.png",
        bgLogo: null
      },
      {
        id: 58,
        name: " 湖北银行宜昌分行",
        code: "HBYCBANK",
        logo: "C:\\home\\bank_logo\\HBYCBANK.png",
        bgLogo: null
      },
      {
        id: 59,
        name: " 台州银行",
        code: "TZCB",
        logo: "C:\\home\\bank_logo\\TZCB.png",
        bgLogo: null
      },
      {
        id: 60,
        name: " 泰安市商业银行",
        code: "TACCB",
        logo: "C:\\home\\bank_logo\\TACCB.png",
        bgLogo: null
      },
      {
        id: 61,
        name: " 许昌银行",
        code: "XCYH",
        logo: "C:\\home\\bank_logo\\XCYH.png",
        bgLogo: null
      },
      {
        id: 62,
        name: " 中国光大银行",
        code: "CEB",
        logo: "C:\\home\\bank_logo\\CEB.png",
        bgLogo: null
      },
      {
        id: 63,
        name: " 宁夏银行",
        code: "NXBANK",
        logo: "C:\\home\\bank_logo\\NXBANK.png",
        bgLogo: null
      },
      {
        id: 64,
        name: " 徽商银行",
        code: "HSBANK",
        logo: "C:\\home\\bank_logo\\HSBANK.png",
        bgLogo: null
      },
      {
        id: 65,
        name: " 九江银行",
        code: "JJBANK",
        logo: "C:\\home\\bank_logo\\JJBANK.png",
        bgLogo: null
      },
      {
        id: 66,
        name: " 农信银清算中心",
        code: "NHQS",
        logo: "C:\\home\\bank_logo\\NHQS.png",
        bgLogo: null
      },
      {
        id: 67,
        name: " 浙江民泰商业银行",
        code: "MTBANK",
        logo: "C:\\home\\bank_logo\\MTBANK.png",
        bgLogo: null
      },
      {
        id: 68,
        name: " 廊坊银行",
        code: "LANGFB",
        logo: "C:\\home\\bank_logo\\LANGFB.png",
        bgLogo: null
      },
      {
        id: 69,
        name: " 鞍山银行",
        code: "ASCB",
        logo: "C:\\home\\bank_logo\\ASCB.png",
        bgLogo: null
      },
      {
        id: 70,
        name: " 昆山农村商业银行",
        code: "KSRB",
        logo: "C:\\home\\bank_logo\\KSRB.png",
        bgLogo: null
      },
      {
        id: 71,
        name: " 玉溪市商业银行",
        code: "YXCCB",
        logo: "C:\\home\\bank_logo\\YXCCB.png",
        bgLogo: null
      },
      {
        id: 72,
        name: " 大连银行",
        code: "DLB",
        logo: "C:\\home\\bank_logo\\DLB.png",
        bgLogo: null
      },
      {
        id: 73,
        name: " 东莞农村商业银行",
        code: "DRCBCL",
        logo: "C:\\home\\bank_logo\\DRCBCL.png",
        bgLogo: null
      },
      {
        id: 74,
        name: " 广州银行",
        code: "GCB",
        logo: "C:\\home\\bank_logo\\GCB.png",
        bgLogo: null
      },
      {
        id: 75,
        name: " 宁波银行",
        code: "NBBANK",
        logo: "C:\\home\\bank_logo\\NBBANK.png",
        bgLogo: null
      },
      {
        id: 76,
        name: " 营口银行",
        code: "BOYK",
        logo: "C:\\home\\bank_logo\\BOYK.png",
        bgLogo: null
      },
      {
        id: 77,
        name: " 陕西信合",
        code: "SXRCCU",
        logo: "C:\\home\\bank_logo\\SXRCCU.png",
        bgLogo: null
      },
      {
        id: 78,
        name: " 桂林银行",
        code: "GLBANK",
        logo: "C:\\home\\bank_logo\\GLBANK.png",
        bgLogo: null
      },
      {
        id: 79,
        name: " 青海银行",
        code: "BOQH",
        logo: "C:\\home\\bank_logo\\BOQH.png",
        bgLogo: null
      },
      {
        id: 80,
        name: " 成都农商银行",
        code: "CDRCB",
        logo: "C:\\home\\bank_logo\\CDRCB.png",
        bgLogo: null
      },
      {
        id: 81,
        name: " 青岛银行",
        code: "QDCCB",
        logo: "C:\\home\\bank_logo\\QDCCB.png",
        bgLogo: null
      },
      {
        id: 82,
        name: " 东亚银行",
        code: "HKBEA",
        logo: "C:\\home\\bank_logo\\HKBEA.png",
        bgLogo: null
      },
      {
        id: 83,
        name: " 湖北银行黄石分行",
        code: "HBHSBANK",
        logo: "C:\\home\\bank_logo\\HBHSBANK.png",
        bgLogo: null
      },
      {
        id: 84,
        name: " 温州银行",
        code: "WZCB",
        logo: "C:\\home\\bank_logo\\WZCB.png",
        bgLogo: null
      },
      {
        id: 85,
        name: " 天津农商银行",
        code: "TRCB",
        logo: "C:\\home\\bank_logo\\TRCB.png",
        bgLogo: null
      },
      {
        id: 86,
        name: " 齐鲁银行",
        code: "QLBANK",
        logo: "C:\\home\\bank_logo\\QLBANK.png",
        bgLogo: null
      },
      {
        id: 87,
        name: " 广东省农村信用社联合社",
        code: "GDRCC",
        logo: "C:\\home\\bank_logo\\GDRCC.png",
        bgLogo: null
      },
      {
        id: 88,
        name: " 浙江泰隆商业银行",
        code: "ZJTLCB",
        logo: "C:\\home\\bank_logo\\ZJTLCB.png",
        bgLogo: null
      },
      {
        id: 89,
        name: " 赣州银行",
        code: "GZB",
        logo: "C:\\home\\bank_logo\\GZB.png",
        bgLogo: null
      },
      {
        id: 90,
        name: " 贵阳市商业银行",
        code: "GYCB",
        logo: "C:\\home\\bank_logo\\GYCB.png",
        bgLogo: null
      },
      {
        id: 91,
        name: " 重庆银行",
        code: "CQBANK",
        logo: "C:\\home\\bank_logo\\CQBANK.png",
        bgLogo: null
      },
      {
        id: 92,
        name: " 龙江银行",
        code: "DAQINGB",
        logo: "C:\\home\\bank_logo\\DAQINGB.png",
        bgLogo: null
      },
      {
        id: 93,
        name: " 南充市商业银行",
        code: "CGNB",
        logo: "C:\\home\\bank_logo\\CGNB.png",
        bgLogo: null
      },
      {
        id: 94,
        name: " 三门峡银行",
        code: "SCCB",
        logo: "C:\\home\\bank_logo\\SCCB.png",
        bgLogo: null
      },
      {
        id: 95,
        name: " 常熟农村商业银行",
        code: "CSRCB",
        logo: "C:\\home\\bank_logo\\CSRCB.png",
        bgLogo: null
      },
      {
        id: 96,
        name: " 上海银行",
        code: "SHBANK",
        logo: "C:\\home\\bank_logo\\SHBANK.png",
        bgLogo: null
      },
      {
        id: 97,
        name: " 吉林银行",
        code: "JLBANK",
        logo: "C:\\home\\bank_logo\\JLBANK.png",
        bgLogo: null
      },
      {
        id: 98,
        name: " 常州农村信用联社",
        code: "CZRCB",
        logo: "C:\\home\\bank_logo\\CZRCB.png",
        bgLogo: null
      },
      {
        id: 99,
        name: " 潍坊银行",
        code: "BANKWF",
        logo: "C:\\home\\bank_logo\\BANKWF.png",
        bgLogo: null
      },
      {
        id: 100,
        name: " 张家港农村商业银行",
        code: "ZRCBANK",
        logo: "C:\\home\\bank_logo\\ZRCBANK.png",
        bgLogo: null
      },
      {
        id: 101,
        name: " 福建海峡银行",
        code: "FJHXBC",
        logo: "C:\\home\\bank_logo\\FJHXBC.png",
        bgLogo: null
      },
      {
        id: 102,
        name: " 浙江省农村信用社联合社",
        code: "ZJNX",
        logo: "C:\\home\\bank_logo\\ZJNX.png",
        bgLogo: null
      },
      {
        id: 103,
        name: " 兰州银行",
        code: "LZYH",
        logo: "C:\\home\\bank_logo\\LZYH.png",
        bgLogo: null
      },
      {
        id: 104,
        name: " 晋商银行",
        code: "JSB",
        logo: "C:\\home\\bank_logo\\JSB.png",
        bgLogo: null
      },
      {
        id: 105,
        name: " 渤海银行",
        code: "BOHAIB",
        logo: "C:\\home\\bank_logo\\BOHAIB.png",
        bgLogo: null
      },
      {
        id: 106,
        name: " 浙江稠州商业银行",
        code: "CZCB",
        logo: "C:\\home\\bank_logo\\CZCB.png",
        bgLogo: null
      },
      {
        id: 107,
        name: " 阳泉银行",
        code: "YQCCB",
        logo: "C:\\home\\bank_logo\\YQCCB.png",
        bgLogo: null
      },
      {
        id: 108,
        name: " 盛京银行",
        code: "SJBANK",
        logo: "C:\\home\\bank_logo\\SJBANK.png",
        bgLogo: null
      },
      {
        id: 109,
        name: " 西安银行",
        code: "XABANK",
        logo: "C:\\home\\bank_logo\\XABANK.png",
        bgLogo: null
      },
      {
        id: 110,
        name: " 包商银行",
        code: "BSB",
        logo: "C:\\home\\bank_logo\\BSB.png",
        bgLogo: null
      },
      {
        id: 111,
        name: " 江苏银行",
        code: "JSBANK",
        logo: "C:\\home\\bank_logo\\JSBANK.png",
        bgLogo: null
      },
      {
        id: 112,
        name: " 抚顺银行",
        code: "FSCB",
        logo: "C:\\home\\bank_logo\\FSCB.png",
        bgLogo: null
      },
      {
        id: 113,
        name: " 河南省农村信用",
        code: "HNRCU",
        logo: "C:\\home\\bank_logo\\HNRCU.png",
        bgLogo: null
      },
      {
        id: 114,
        name: " 交通银行",
        code: "COMM",
        logo: "C:\\home\\bank_logo\\COMM.png",
        bgLogo: null
      },
      {
        id: 115,
        name: " 邢台银行",
        code: "XTB",
        logo: "C:\\home\\bank_logo\\XTB.png",
        bgLogo: null
      },
      {
        id: 116,
        name: " 中信银行",
        code: "CITIC",
        logo: "C:\\home\\bank_logo\\CITIC.png",
        bgLogo: null
      },
      {
        id: 117,
        name: " 华夏银行",
        code: "HXBANK",
        logo: "C:\\home\\bank_logo\\HXBANK.png",
        bgLogo: null
      },
      {
        id: 118,
        name: " 湖南省农村信用社",
        code: "HNRCC",
        logo: "C:\\home\\bank_logo\\HNRCC.png",
        bgLogo: null
      },
      {
        id: 119,
        name: " 东营市商业银行",
        code: "DYCCB",
        logo: "C:\\home\\bank_logo\\DYCCB.png",
        bgLogo: null
      },
      {
        id: 120,
        name: " 鄂尔多斯银行",
        code: "ORBANK",
        logo: "C:\\home\\bank_logo\\ORBANK.png",
        bgLogo: null
      },
      {
        id: 121,
        name: " 北京农村商业银行",
        code: "BJRCB",
        logo: "C:\\home\\bank_logo\\BJRCB.png",
        bgLogo: null
      },
      {
        id: 122,
        name: " 信阳银行",
        code: "XYBANK",
        logo: "C:\\home\\bank_logo\\XYBANK.png",
        bgLogo: null
      },
      {
        id: 123,
        name: " 自贡市商业银行",
        code: "ZGCCB",
        logo: "C:\\home\\bank_logo\\ZGCCB.png",
        bgLogo: null
      },
      {
        id: 124,
        name: " 成都银行",
        code: "CDCB",
        logo: "C:\\home\\bank_logo\\CDCB.png",
        bgLogo: null
      },
      {
        id: 125,
        name: " 韩亚银行",
        code: "HANABANK",
        logo: "C:\\home\\bank_logo\\HANABANK.png",
        bgLogo: null
      },
      {
        id: 126,
        name: " 中国民生银行",
        code: "CMBC",
        logo: "C:\\home\\bank_logo\\CMBC.png",
        bgLogo: null
      },
      {
        id: 127,
        name: " 洛阳银行",
        code: "LYBANK",
        logo: "C:\\home\\bank_logo\\LYBANK.png",
        bgLogo: null
      },
      {
        id: 128,
        name: " 广东发展银行",
        code: "GDB",
        logo: "C:\\home\\bank_logo\\GDB.png",
        bgLogo: null
      },
      {
        id: 129,
        name: " 齐商银行",
        code: "ZBCB",
        logo: "C:\\home\\bank_logo\\ZBCB.png",
        bgLogo: null
      },
      {
        id: 130,
        name: " 开封市商业银行",
        code: "CBKF",
        logo: "C:\\home\\bank_logo\\CBKF.png",
        bgLogo: null
      },
      {
        id: 131,
        name: " 内蒙古银行",
        code: "H3CB",
        logo: "C:\\home\\bank_logo\\H3CB.png",
        bgLogo: null
      },
      {
        id: 132,
        name: " 兴业银行",
        code: "CIB",
        logo: "C:\\home\\bank_logo\\CIB.png",
        bgLogo: null
      },
      {
        id: 133,
        name: " 重庆农村商业银行",
        code: "CRCBANK",
        logo: "C:\\home\\bank_logo\\CRCBANK.png",
        bgLogo: null
      },
      {
        id: 134,
        name: " 石嘴山银行",
        code: "SZSBK",
        logo: "C:\\home\\bank_logo\\SZSBK.png",
        bgLogo: null
      },
      {
        id: 135,
        name: " 德州银行",
        code: "DZBANK",
        logo: "C:\\home\\bank_logo\\DZBANK.png",
        bgLogo: null
      },
      {
        id: 136,
        name: " 上饶银行",
        code: "SRBANK",
        logo: "C:\\home\\bank_logo\\SRBANK.png",
        bgLogo: null
      },
      {
        id: 137,
        name: " 乐山市商业银行",
        code: "LSCCB",
        logo: "C:\\home\\bank_logo\\LSCCB.png",
        bgLogo: null
      },
      {
        id: 138,
        name: " 江西省农村信用",
        code: "JXRCU",
        logo: "C:\\home\\bank_logo\\JXRCU.png",
        bgLogo: null
      },
      {
        id: 139,
        name: " 中国工商银行",
        code: "ICBC",
        logo: "C:\\home\\bank_logo\\ICBC.png",
        bgLogo: null
      },
      {
        id: 140,
        name: " 晋中市商业银行",
        code: "JZBANK",
        logo: "C:\\home\\bank_logo\\JZBANK.png",
        bgLogo: null
      },
      {
        id: 141,
        name: " 湖州市商业银行",
        code: "HZCCB",
        logo: "C:\\home\\bank_logo\\HZCCB.png",
        bgLogo: null
      },
      {
        id: 142,
        name: " 南海农村信用联社",
        code: "NHB",
        logo: "C:\\home\\bank_logo\\NHB.png",
        bgLogo: null
      },
      {
        id: 143,
        name: " 新乡银行",
        code: "XXBANK",
        logo: "C:\\home\\bank_logo\\XXBANK.png",
        bgLogo: null
      },
      {
        id: 144,
        name: " 江苏江阴农村商业银行",
        code: "JRCB",
        logo: "C:\\home\\bank_logo\\JRCB.png",
        bgLogo: null
      },
      {
        id: 145,
        name: " 云南省农村信用社",
        code: "YNRCC",
        logo: "C:\\home\\bank_logo\\YNRCC.png",
        bgLogo: null
      },
      {
        id: 146,
        name: " 中国农业银行",
        code: "ABC",
        logo: "C:\\home\\bank_logo\\ABC.png",
        bgLogo: null
      },
      {
        id: 147,
        name: " 广西省农村信用",
        code: "GXRCU",
        logo: "C:\\home\\bank_logo\\GXRCU.png",
        bgLogo: null
      },
      {
        id: 148,
        name: " 中国邮政储蓄银行",
        code: "PSBC",
        logo: "C:\\home\\bank_logo\\PSBC.png",
        bgLogo: null
      },
      {
        id: 149,
        name: " 驻马店银行",
        code: "BZMD",
        logo: "C:\\home\\bank_logo\\BZMD.png",
        bgLogo: null
      },
      {
        id: 150,
        name: " 安徽省农村信用社",
        code: "ARCU",
        logo: "C:\\home\\bank_logo\\ARCU.png",
        bgLogo: null
      },
      {
        id: 151,
        name: " 甘肃省农村信用",
        code: "GSRCU",
        logo: "C:\\home\\bank_logo\\GSRCU.png",
        bgLogo: null
      },
      {
        id: 152,
        name: " 辽阳市商业银行",
        code: "LYCB",
        logo: "C:\\home\\bank_logo\\LYCB.png",
        bgLogo: null
      },
      {
        id: 153,
        name: " 吉林农信",
        code: "JLRCU",
        logo: "C:\\home\\bank_logo\\JLRCU.png",
        bgLogo: null
      },
      {
        id: 154,
        name: " 乌鲁木齐市商业银行",
        code: "URMQCCB",
        logo: "C:\\home\\bank_logo\\URMQCCB.png",
        bgLogo: null
      },
      {
        id: 155,
        name: " 中山小榄村镇银行",
        code: "XLBANK",
        logo: "C:\\home\\bank_logo\\XLBANK.png",
        bgLogo: null
      },
      {
        id: 156,
        name: " 长沙银行",
        code: "CSCB",
        logo: "C:\\home\\bank_logo\\CSCB.png",
        bgLogo: null
      },
      {
        id: 157,
        name: " 金华银行",
        code: "JHBANK",
        logo: "C:\\home\\bank_logo\\JHBANK.png",
        bgLogo: null
      },
      {
        id: 158,
        name: " 河北银行",
        code: "BHB",
        logo: "C:\\home\\bank_logo\\BHB.png",
        bgLogo: null
      },
      {
        id: 159,
        name: " 鄞州银行",
        code: "NBYZ",
        logo: "C:\\home\\bank_logo\\NBYZ.png",
        bgLogo: null
      },
      {
        id: 160,
        name: " 临商银行",
        code: "LSBC",
        logo: "C:\\home\\bank_logo\\LSBC.png",
        bgLogo: null
      },
      {
        id: 161,
        name: " 承德银行",
        code: "BOCD",
        logo: "C:\\home\\bank_logo\\BOCD.png",
        bgLogo: null
      },
      {
        id: 162,
        name: " 山东农信",
        code: "SDRCU",
        logo: "C:\\home\\bank_logo\\SDRCU.png",
        bgLogo: null
      },
      {
        id: 163,
        name: " 南昌银行",
        code: "NCB",
        logo: "C:\\home\\bank_logo\\NCB.png",
        bgLogo: null
      },
      {
        id: 164,
        name: " 天津银行",
        code: "TCCB",
        logo: "C:\\home\\bank_logo\\TCCB.png",
        bgLogo: null
      },
      {
        id: 165,
        name: " 吴江农商银行",
        code: "WJRCB",
        logo: "C:\\home\\bank_logo\\WJRCB.png",
        bgLogo: null
      },
      {
        id: 166,
        name: " 城市商业银行资金清算中心",
        code: "CBBQS",
        logo: "C:\\home\\bank_logo\\CBBQS.png",
        bgLogo: null
      },
      {
        id: 167,
        name: " 河北省农村信用社",
        code: "HBRCU",
        logo: "C:\\home\\bank_logo\\HBRCU.png",
        bgLogo: null
      }
    ],
    msg: "删除成功"
  });
  Mock.mock(/manage\/bankcard\/info\/%7BcardNo%7D/, "get", {
    code: 200,
    data: {
      id: 114,
      name: " 交通银行",
      code: "COMM",
      logo: "C:\\home\\bank_logo\\COMM.png",
      bgLogo: null
    },
    msg: "删除成功"
  });

  Mock.mock(/\S+/, {});
}

export default Mock;
