const json = [
  {
    src: require("../../assets/images/logo/5-20-2020/bank (1).png"),
    name: "logo.pay_h1",
    ename: "Bank",
    info: "logo.pay_p1",
    einfo: "e-e-e-e-e-"
  },
  {
    src: require("../../assets/images/logo/5-20-2020/bank (2).png"),
    name: "logo.pay_h2",
    ename: "AliPay",
    info: "logo.pay_p2",
    einfo: "ali-pay-ali"
  },
  {
    src: require("../../assets/images/logo/5-20-2020/bank (3).png"),
    name: "logo.pay_h9",
    ename: "WeChat",
    info: "logo.pay_p2",
    einfo: "ali-we-ali"
  },
  {
    src: require("../../assets/images/logo/5-20-2020/bank (4).png"),
    name: "logo.pay_h3",
    ename: "WeChat",
    info: "logo.pay_p4",
    einfo: "ali-we-ali"
  },
  {
    src: require("../../assets/images/logo/5-20-2020/bank (5).png"),
    name: "logo.pay_h4",
    ename: "WeChat",
    info: "logo.pay_p5",
    einfo: "ali-we-ali"
  },
  {
    src: require("../../assets/images/logo/5-20-2020/bank (6).png"),
    name: "logo.pay_h5",
    ename: "WeChat",
    info: "logo.pay_p6",
    einfo: "ali-we-ali"
  },
  {
    src: require("../../assets/images/logo/5-20-2020/bank (7).png"),
    name: "logo.pay_h6",
    ename: "WeChat",
    info: "logo.pay_p3",
    einfo: "ali-we-ali"
  },
  {
    src: require("../../assets/images/logo/5-20-2020/bank (8).png"),
    name: "logo.pay_h10",
    ename: "",
    info: "",
    einfo: "ali-we-ali"
  }
];

export default json;
