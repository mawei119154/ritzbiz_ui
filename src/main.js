import "core-js";
import Vue from "vue";
import App from "./App.vue";
import i18n from "./locales/index";
import router from "./router";
import store from "./store";
import "./permission";
import cssVars from "css-vars-ponyfill";
import "./assets/css/reset.scss";
import "./assets/css/hui.less";
import "./assets/css/skt.scss";
import "./assets/css/font.css";
import "./registerServiceWorker";
import animated from "animate.css";
// import "core-js/stable";
import { install2 } from "./plugin/index";

import "amfe-flexible";

Vue.use(animated);

import {
  install,
  Prototypes,
  DatePicker,
  Table,
  TableItem,
  Pagination
} from "heyui";

import "./registerServiceWorker";
import "./registerServiceWorker";

Vue.use(install, {
  components: { DatePicker, Table, TableItem, Pagination },
  prototypes: Prototypes
});
// Vue.config.errorHandler = (msg, vm, trace) => {
//   console.error("通过vue errorHandler捕获的错误");
//   console.error(msg);
//   console.error(vm);
//   console.error(trace);
// };

// import './mock' // simulation data

cssVars({
  // watch: true,
  onlyLegacy: true,

  // Targets
  rootElement: document, // default
  shadowDOM: true,
  // Sources
  include: "link[rel=stylesheet],style",
  exclude: "",
  variables: {},
  // Options
  // onlyLegacy: true,
  preserveStatic: true,
  preserveVars: true,
  silent: true,
  updateDOM: true,
  updateURLs: true,
  watch: true
});

Vue.use(install2, {
  Notification: true,
  Modal: true
});

Vue.config.productionTip = false;

// router.beforeEach((to, from, next) => {
//   // ...
//   console.log(1)
//   next()
// })

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
