import modalIntall from "./lib/modal";
import loadingBarInstall from "./lib/loading-bar";
import notificationInstall from "./lib/notification";
import previewInstall from "./lib/preview";

// /*eslint-disable*/
// ;(function (global, factory) {
//     typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
//     typeof define === 'function' && define.amd ? define(factory) :
//     (global.RadonUI = factory())
//     // factory()
// }(this, function () {
// /*eslint-enable*/
//     'use strict'
//     function install (Vue, options = {
//         Modal: true,
//         Notification: true,
//         Preview: true,
//         LoadingBar: true
//     }) {
//         const RADON_EVENT_BUS = new Vue({})
//         window.RADON_EVENT_BUS = RADON_EVENT_BUS
//         Vue.use(require('vue-animated-list'))

//         const RadonInit = (vm) => {
//             if (options.Modal) {
//                 modalIntall(Vue, vm)
//             }

//             if (options.Notification) {
//                 notificationInstall(Vue, vm)
//             }

//             if (options.LoadingBar) {
//                 loadingBarInstall(Vue, vm)
//             }

//             if (options.Preview) {
//                 previewInstall(Vue, vm)
//             }
//         }

//         RadonInit(RADON_EVENT_BUS)
//     }
//     return install
// }))

export function install2(
  Vue,
  options = {
    Modal: true,
    Notification: true,
    Preview: true,
    LoadingBar: true
  }
) {
  const RADON_EVENT_BUS = new Vue({
    data: {
      RADON_NOTIFICATION: [],
      RADON_MODAL: {
        show: false,
        title: "",
        content: "",
        rawContent: "",
        cancel: () => {},
        confirm: () => {},
        large: false,
        cancelButton: {
          show: true,
          type: "",
          text: "取消"
        },
        confirmButton: {
          show: true,
          type: "primary",
          text: "确定"
        }
      }
    }
  });
  window.RADON_EVENT_BUS = RADON_EVENT_BUS;
  // Vue.RADON_EVENT_BUS = RADON_EVENT_BUS
  const RadonInit = vm => {
    if (options.Modal) {
      modalIntall(Vue, vm);
    }

    if (options.Notification) {
      notificationInstall(Vue, vm);
    }

    if (options.LoadingBar) {
      loadingBarInstall(Vue, vm);
    }

    if (options.Preview) {
      previewInstall(Vue, vm);
    }
  };

  RadonInit(RADON_EVENT_BUS);
}
