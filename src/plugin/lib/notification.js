import { rdNotification } from "../index";

export default (Vue, $root) => {
  // Vue.set($root, 'RADON_NOTIFICATION', [])
  // window.RADON_EVENT_BUS.RADON_NOTIFICATION = $root['RADON_NOTIFICATION']= []
  // console.log($root.RADON_NOTIFICATION)
  Vue.component("rd-notification", rdNotification);
  let index = 1;
  window.$Notification = Vue.prototype.$Notification = {
    remove(item, duration) {
      let k = setTimeout(() => {
        // console.log($root.RADON_NOTIFICATION,'删除之气爱你')
        // $root.RADON_NOTIFICATION.delete(item)
        if (
          $root.RADON_NOTIFICATION.findIndex(ite => ite.id === item.id) >= 0
        ) {
          $root.RADON_NOTIFICATION.splice(
            $root.RADON_NOTIFICATION.findIndex(ite => ite.id === item.id),
            1
          );
        }
        clearTimeout(k);
        // $root.RADON_NOTIFICATION.splice(item.index,1)
        // window.RADON_EVENT_BUS.RADON_NOTIFICATION = $root.RADON_NOTIFICATION
      }, duration);
    },
    create(type, title, content, duration) {
      let item = {
        id: index,
        type: type,
        title: title,
        content: content
      };
      $root.RADON_NOTIFICATION.push(item);
      index += 1;
      // window.RADON_EVENT_BUS.RADON_NOTIFICATION = $root.RADON_NOTIFICATION
      if (duration) {
        this.remove(item, duration);
      }
    },
    success(title, content, duration) {
      this.create("success", title, content, duration);
    },
    info(title, content, duration) {
      this.create("info", title, content, duration);
    },
    warning(title, content, duration) {
      this.create("warning", title, content, duration);
    },
    failed(title, content, duration) {
      this.create("failed", title, content, duration);
    }
  };
};
