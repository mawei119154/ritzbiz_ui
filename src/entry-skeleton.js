import Vue from "vue";
import Skeleton from "./Skeleton.vue";

export default new Vue({
  components: {
    Skeleton
  },
  template: `
  <div style='height: 100%;'>
      <skeleton id="skeleton" style="height:100%"/>
      <skeleton1 id="skeleton1" style="display:none"/>
      <skeleton2 id="skeleton2" style="display:none"/>
  </div>
`
});
