const getters = {
  language: state => state.app.language,
  user: state => state.app.user,
  connected: state => state.app.connected,
  order_id: state => state.app.order_id,
  roomId: state => state.app.room_id,
  loginAccount: state => state.app.loginAccount,
  user2: state => state.app.user,
  currencyList: state => state.list.currencyList,
  dictListYuan: state => state.list.dictListYuan,
  dictListTu: state => state.list.dictListTu,
  couponList: state => state.list.couponList
};

export default getters;
