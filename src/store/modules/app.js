const app = {
  state: {
    language: navigator.language == "zh-CN" ? "zh" : "en",
    user: null,
    loginAccount: "",
    connected: false,
    order_id: "",
    room_id: ""
  },
  mutations: {
    SET_LANGUAGE: (state, language) => {
      state.language = language;
      // Cookies.set('language', language)
    },
    SET_USER: (state, user) => {
      state.user = user;
    },
    SET_LOGIN_ACCOUNT: (state, account) => {
      state.loginAccount = account;
    },
    SET_CONNECT_STATUS: (state, status) => {
      state.connected = status;
    },
    SET_ORDER_ID: (state, id) => {
      state.order_id = id;
    },
    SET_ROOM_ID: (state, id) => {
      state.room_id = id;
    }
  },
  actions: {
    setLanguage({ commit }, language) {
      commit("SET_LANGUAGE", language);
    },
    setLoginAccount({ commit }, account) {
      commit("SET_LOGIN_ACCOUNT", account);
    },
    setUser({ commit }, user) {
      commit("SET_USER", user);
      return user;
    },
    setRoomId({ commit }, status) {
      commit("SET_ROOM_ID", status);
    }
  }
};

export default app;
