const list = {
  state: {
    currencyList: [],
    dictListYuan: [],
    dictListTu: [],
    couponList: []
  },
  mutations: {
    SET_CURRENCYLIST: (state, item) => {
      state.currencyList = item;
    },
    SET_DICTLIST_YUAN: (state, item) => {
      state.dictListYuan = item;
    },
    SET_DICTLIST_TU: (state, item) => {
      state.dictListTu = item;
    },
    SET_COUPONLIST: (state, item) => {
      state.couponList = item;
    }
  },
  actions: {
    setCurrency({ commit }, item) {
      commit("SET_CURRENCYLIST", item);
    },
    setDictYuan({ commit }, item) {
      commit("SET_DICTLIST_YUAN", item);
      return item;
    },
    setDictTu({ commit }, item) {
      commit("SET_DICTLIST_TU", item);
      return item;
    },
    setCoupon({ commit }, item) {
      commit("SET_COUPONLIST", item);
      return item;
    }
  }
};

export default list;
