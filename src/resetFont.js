// 基准大小
const baseSize = 16;
// 设置 rem 函数
function setRem() {
  // 当前页面宽度相对于 800 宽的缩放比例，可根据自己需要修改。
  let scale = (document.documentElement.clientWidth - 800) / (1920 - 800);
  scale = scale < 0 ? 0 : scale;
  // 设置页面根节点字体大小
  document.documentElement.style.fontSize = 12 + (baseSize - 12) * scale + "px";
  console.log(document.documentElement.style.fontSize);
}
// 初始化
setRem();
// 改变窗口大小时重新设置 rem
window.onresize = function() {
  setRem();
};
