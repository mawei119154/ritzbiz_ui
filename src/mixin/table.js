import ListQuerys from "../components/ListQuery.vue";
import { debounce } from "../utils/index";
export default {
  components: {
    ListQuerys
  },
  data() {
    return {
      loading: true,
      rows: 10, // pageSize
      page: 1, // currentPage
      count: 0,
      pagination: {
        page: 1,
        size: 10,
        total: 0
      },
      tableData: [],
      multipleSelection: [],
      querys: {
        dateRange: ["", ""],
        sele: ""
      },
      sortObj: {
        key: "",
        bool: ""
      },
      formData: {},
      formVisible: false,
      editType: ""
    };
  },
  computed: {
    pathParam() {
      return this.tableFunc.keys || [];
    }
  },
  watch: {
    rows: function() {
      this.pagination = {
        page: this.page,
        size: this.rows,
        total: this.count
      };
      let o = this.rows;
      setTimeout(() => {
        // 如果当前页码没有变化，则请求数据
        if (this.rows == o) this.getList();
      }, 0);
    },
    page: function() {
      this.pagination = {
        page: this.page,
        size: this.rows,
        total: this.count
      };
      this.getList();
    },
    count: function() {
      this.pagination = {
        page: this.page,
        size: this.rows,
        total: this.count
      };
      // this.getList();
    },
    formVisible: function(n) {
      this.$nextTick(() => {
        if (n) this.$refs.formData && this.$refs.formData.clearValidate();
      });
    },
    querys: {
      handler() {
        this.page = 1;
        this.getList();
      },
      deep: true
    }
  },
  created() {
    this.getList = debounce(this.getList, 500);
  },
  mounted() {
    this.getList();
    // this.calcHeight();
    // window.addEventListener("resize", this.calcHeight);
  },
  methods: {
    paramsFormat() {
      let param = {
        page: this.page,
        rows: this.rows,
        ...this.tableFunc.params
      };
      // console.log(param)
      for (let key in this.querys) {
        let data = this.querys[key];
        if (key == "dateRange" && data) {
          // 所有date组件需采用value-format
          if (data[0]) param.startTime = data[0];
          if (data[1]) param.endTime = data[1];
        } else if (data !== undefined && data !== "" && data !== null) {
          param[key] = data;
        }
      }
      if (this.sortObj.key && this.sortObj.bool !== "") {
        param[this.sortObj.key] = this.sortObj.bool;
      }
      return param;
    },
    handleSelectionChange(val) {
      this.multipleSelection = val;
    },
    handleSizeChange(val) {
      this.rows = val;
    },
    handleCurrentChange(val) {
      // this.page = val;
      this.rows = val.size;
      this.page = val.page;
      this.count = val.total;
    },
    orderBy: function({ column, prop, order }) {
      console.log(column, "不知道");
      if (prop) {
        this.sortObj.key = prop + "_sort";
        this.sortObj.bool = order === "ascending";
      } else {
        this.sortObj.key = "";
        this.sortObj.bool = "";
      }
      this.handleQuery();
    },
    handleQuery() {
      this.page = 1;
      this.getList();
    },
    exportFun() {
      this.tableFunc.export(this.paramsFormat()).then(res => {
        let head = res.headers["content-disposition"];
        console.log(head);
        let head1 = head.split('filename="')[1].split('"')[0];
        let head2 = head.split('filename*="')[1];
        if (head2) {
          head2 = head2.split('"')[0];
          head1 = decodeURIComponent(head2.split("''")[1]);
        }
        var content = res;
        var data = new Blob([content], {
          type: "application/vnd.ms-excel,charset=utf-8"
        });
        var downloadUrl = window.URL.createObjectURL(data);
        var anchor = document.createElement("a");
        anchor.href = downloadUrl;
        anchor.download = head1;
        anchor.click();
        window.URL.revokeObjectURL(data);
      });
    },
    async getList() {
      if (!this.tableFunc) return;
      this.multipleSelection = [];
      try {
        this.loading = true;
        this.tableData = [];
        // console.log(this.tableFunc.get())
        let res = await this.tableFunc.post(
          ...this.pathParam,
          this.paramsFormat()
        );
        // console.log(res.data.list)
        // res[this.tableFunc.resKey]
        if (res.data.list.length) {
          this.tableData = res.data.list;
          this.count = res.data.total || this.count;
        } else if (this.page !== 1) {
          this.page--;
        } else {
          this.tableData = [];
          this.count = 0;
        }
      } catch (err) {
        console.log("获取数据失败", err);
      }
      this.loading = false;
    }
  }
};
