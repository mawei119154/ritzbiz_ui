import { bus } from "../utils/index";
import { mapGetters } from "vuex";
export default {
  data() {
    return {};
  },
  computed: {
    ...mapGetters(["user"])
  },
  methods: {
    funcOn() {
      bus.$on("");
    }
  }
};
