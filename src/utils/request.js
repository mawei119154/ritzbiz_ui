import axios from "axios";
import store from "../store/index";
//在main.js设置全局的请求次数，请求的间隙
axios.defaults.retry = 4;
axios.defaults.retryDelay = 1000;
// axios.defaults.withCredentials = true;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
axios.defaults.headers.post["Content-Type"] = "application/json";
axios.defaults.transformRequest = [
  function(data) {
    // post 参数是空 直接返回
    if (data == undefined) return data;
    // console.log(data, typeof data.append == "function");
    // post 是 上传文件
    if (typeof data.append == "function") {
      return data;
    }
    // post 是要求 application/x-www-form-urlencoded
    let ret = "";
    // for (let it in data) {
    //   ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
    // }
    // post 是要求  'application/json'
    ret = JSON.stringify(data);
    return ret;
    // return data
  }
];

// console.log(process.env.BASE_API,'基本路径')
const service = axios.create({
  // baseURL: process.env.BASE_API, // api 的 base_url process.env.BASE_API   'http://127.0.0.1:8085'
  baseURL: "/", // api 的 base_url process.env.BASE_API   'http://127.0.0.1:8085'
  timeout: 10000000 // request timeout
});

service.interceptors.request.use(
  config => {
    if (config.method === "get") {
      let dataStr = ""; // 数据拼接字符串
      config.params &&
        Object.keys(config.params).forEach(key => {
          dataStr += key + "=" + config.params[key] + "&";
        });
      if (dataStr !== "") {
        dataStr = dataStr.substr(0, dataStr.lastIndexOf("&"));
        config.url = config.url + "?" + dataStr;
        config.params = [];
        // console.log(config.url)
      }
    }
    // Do something before request is sent
    // if (store.getters.token) {
    // // 让每个请求携带token-- ['X-Token']为自定义key 请根据实际情况自行修改
    //     config.headers['X-Token'] = getToken()
    // }
    return config;
  },
  error => {
    // Do something with request error
    console.log("err-request" + error); // for debug

    // Promise.reject(error)

    var config = error.config;
    // If config does not exist or the retry option is not set, reject
    if (!config || !config.retry) return Promise.reject(error);

    // Set the variable for keeping track of the retry count
    config.__retryCount = config.__retryCount || 0;

    // Check if we've maxed out the total number of retries
    if (config.__retryCount >= config.retry) {
      // Reject with the error
      return Promise.reject(error);
    }

    // Increase the retry count
    config.__retryCount += 1;

    // Create new promise to handle exponential backoff
    var backoff = new Promise(function(resolve) {
      setTimeout(function() {
        resolve();
      }, config.retryDelay || 1);
    });

    // Return the promise in which recalls axios to retry the request
    return backoff.then(function() {
      return service(config);
    });
  }
);

// response interceptor
service.interceptors.response.use(
  response => {
    // console.log(response.config.url, '----', response.data, response)
    if (response.data.code == 407) {
      window.$Notification.failed("", "登录过期", 3000);
      store.dispatch("setUser", null);
      localStorage.setItem("token", "");
      window.location.replace(window.location.origin + "/unlock/login");
    }
    response.data.headers = response.headers;
    return response.data;
  },
  error => {
    console.log("error.response----", error.response);
    if (error.response.data === "") {
      window.$Notification.failed("", error.response.statusText, 4000);
    } else {
      window.$Notification.failed("", error.response.data, 4000);
    }
    return Promise.reject(error);
  }
);

export default service;
