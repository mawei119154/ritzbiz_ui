// js将手机号身份证及银行卡号等敏感信息替换为*号
// param..replace(/^(.{6})(?:\d+)(.{4})$/,"$1******$2");

// 银行卡号都是16-19位的，所以这里限制了最大输入位数是23位，为什么是23位
//  '6201222200003334567'.replace(/(\d{4})(?=\d)/g, "$1 "); 输入的实现 填入 空格
import Vue from "vue";
const EventBus = new Vue();
import { getReceiptWayCountry } from "../api/payMethods";

// 比如搜索框，用户在输入的时候使用change事件去调用搜索，如果用户每一次输入都去搜索的话，那得消耗多大的服务器资源
// 节流函数  比 防抖 要宽松些，这时我们不想用户一味的输入，而是给用户一些搜索提示，所以在当中限制每过500ms就查询一次此时的String，这就是节流。
// 原理：节流函数不管事件触发有多频繁，都会保证在规定时间内一定会执行一次真正的事件处理函数。
function throttle(callback, limit) {
  var wait = false;
  return function() {
    const context = this;
    let args = arguments;
    if (!wait) {
      // callback.call();
      callback.apply(context, args);
      wait = true;
      setTimeout(function() {
        wait = false;
      }, limit);
    }
  };
}
// 方法二：时间戳
const throttle2 = function(fn, delay) {
  let preTime = Date.now();

  return function() {
    const context = this;
    let args = arguments;
    let doTime = Date.now();
    if (doTime - preTime >= delay) {
      fn.apply(context, args);
      preTime = Date.now();
    }
  };
};
// 方法三：综合使用时间戳与定时器，完成一个事件触发时立即执行，触发完毕还能执行一次的节流函数
function throttle3(func, delay) {
  let timer = null;
  let startTime = Date.now();

  return function() {
    let curTime = Date.now();
    let remaining = delay - (curTime - startTime);
    const context = this;
    const args = arguments;

    clearTimeout(timer);
    if (remaining <= 0) {
      func.apply(context, args);
      startTime = Date.now();
    } else {
      timer = setTimeout(func, remaining);
    }
  };
}
// 防抖函数  原理：将若干个函数调用合成为一次，并在给定时间过去之后仅被调用一次。
function debounce(fn, delay) {
  let timer;
  return function() {
    // 通过 ‘this’ 和 ‘arguments’ 获取函数的作用域和变量
    let context = this;
    let args = arguments;
    // 清理掉正在执行的函数，并重新执行
    clearTimeout(timer);
    timer = setTimeout(function() {
      fn.apply(context, args);
    }, delay);
  };
}
function debounceFinal(func, wait, immediate) {
  var timeout, args, context, timestamp, result;

  var later = function() {
    // 据上一次触发时间间隔
    var last = Date.now() - timestamp;

    // 上次被包装函数被调用时间间隔last小于设定时间间隔wait
    if (last < wait && last > 0) {
      timeout = setTimeout(later, wait - last);
    } else {
      timeout = null;
      // 如果设定为immediate===true，因为开始边界已经调用过了此处无需调用
      if (!immediate) {
        result = func.apply(context, args);
        if (!timeout) context = args = null;
      }
    }
  };

  return function() {
    context = this;
    args = arguments;
    timestamp = Date.now();
    var callNow = immediate && !timeout;
    // 如果延时不存在，重新设定延时
    if (!timeout) timeout = setTimeout(later, wait);
    if (callNow) {
      result = func.apply(context, args);
      context = args = null;
    }

    return result;
  };
}

const pad = val => {
  val = Math.floor(val);
  if (val < 10) {
    return "0" + val;
  }
  return val + "";
};

const ICON_MAP = {
  success: "ion-checkmark-circled",
  failed: "ion-close-circled",
  warning: "ion-alert-circled",
  info: "ion-information-circled",
  loading: "ion-load-a"
};

const catIn = function(target, parent) {
  let path = [];
  let parentNode = target;
  while (parentNode && parentNode !== document.body) {
    path.push(parentNode);
    parentNode = parentNode.parentNode;
  }
  return path.indexOf(parent) !== -1;
};

function resetTokenAndClearUser() {
  // 退出登陆 清除用户资料
  localStorage.setItem("token", "");
  localStorage.setItem("userImg", "");
  localStorage.setItem("userName", "");
  // 重设路由
  //   resetRouter();
}
// 判断闰年
function leapYear(year) {
  return !(year % (year % 100 ? 4 : 400));
}
/**
 *
 * @param {*} supportsWebp 判断浏览器是否支持webp
 */
// const supportsWebp = ({ createImageBitmap, Image }) => {
const supportsWebp = () => {
  if (!window.createImageBitmap || !Image) return Promise.resolve(false);

  return new Promise(resolve => {
    const image = new Image();
    image.onload = () => {
      window
        .createImageBitmap(image)
        .then(() => {
          resolve(true);
        })
        .catch(() => {
          resolve(false);
        });
    };
    image.onerror = () => {
      resolve(false);
    };
    image.src =
      "data:image/webp;base64,UklGRh4AAABXRUJQVlA4TBEAAAAvAAAAAAfQ//73v/+BiOh/AAA=";
  });
};
const supportsWebpInit = () => {
  if (!window.createImageBitmap || !Image) return false;

  return (() => {
    const image = new Image();
    image.onload = () => {
      window
        .createImageBitmap(image)
        .then(() => {
          return true;
        })
        .catch(() => {
          return false;
        });
    };
    image.onerror = () => {
      return false;
    };
    image.src =
      "data:image/webp;base64,UklGRh4AAABXRUJQVlA4TBEAAAAvAAAAAAfQ//73v/+BiOh/AAA=";
  })();
};

/**
 * 将图片转化成其他格式
 * @param {*} image 图片
 * @param {*} format 格式
 */
const convertImage = (image, format) => {
  let canvas = document.createElement("canvas");
  let context2D = canvas.getContext("2d");

  context2D.clearRect(0, 0, canvas.width, canvas.height);
  canvas.width = image.naturalWidth;
  canvas.height = image.naturalHeight;
  image.crossOrigin = "Anonymous";
  context2D.drawImage(image, 0, 0);
  return canvas.toDataURL("image/" + (format || "png"), 1);
};

/**
 * 检查权限 以便 是否弹窗拦截
 * @param {*} item.type 触发时的环境 1添加收款方式pmAdd 2 买入卖出trade  3 触发提币withdraw 4 支付订单的时候 payment
 * @param {*} item.isBuy 买入卖出  1 in  0 out
//  * @param {*} item.tradeOutPm 卖出  检测当前国家的收款方式 有没有
 * @param {*} item.countryInfoId 卖出  当前国家的id
 * @param {*} user 用户信息
 */
const checkPermissions = async (item, user) => {
  let securityBindingNum = 0; // 安全绑定的个数
  let fundPss = true; // 资金密码是否设置
  let idAuth = true; // 身份验证是否完成
  // let currentCountryPm = item.tradeOutPm === undefined ? item.tradeOutPm : true; // 如果没有这个参数就是 不用判断这个条件 默认 true
  let currentCountryPm = true; // 如果没有这个参数就是 不用判断这个条件 默认 true
  switch (item.type) {
    case "pmAdd":
      // if (user.email != null) {
      //   securityBindingNum += 1;
      // }
      // if (user.phone != null) {
      //   securityBindingNum += 1;
      // }
      // if (user.googleAuthTag === "1") {
      //   securityBindingNum += 1;
      // }
      if (user.identityAuthStatus === "2") {
        idAuth = true;
      } else {
        idAuth = false;
      }
      if (user.ifPayPassword === "1") {
        fundPss = true;
      } else {
        fundPss = false;
      }
      break;
    case "withdraw":
      if (user.email != null) {
        securityBindingNum += 1;
      }
      if (user.phone != null) {
        securityBindingNum += 1;
      }
      if (user.googleAuthTag === "1") {
        securityBindingNum += 1;
      }
      if (user.identityAuthStatus === "2") {
        idAuth = true;
      } else {
        idAuth = false;
      }
      if (user.ifPayPassword === "1") {
        fundPss = true;
      } else {
        fundPss = false;
      }
      break;
    case "trade":
      if (user.email != null) {
        securityBindingNum += 1;
      }
      if (user.phone != null) {
        securityBindingNum += 1;
      }
      if (user.googleAuthTag === "1") {
        securityBindingNum += 1;
      }
      if (user.identityAuthStatus === "2") {
        idAuth = true;
      } else {
        idAuth = false;
      }
      if (item.isBuy == "1") {
        // 买入
        if (user.ifPayPassword === "1") {
          fundPss = true;
        } else {
          fundPss = false;
        }
      } else {
        // 卖出时 还要考虑是否有 当前国家的收款方式
        await getReceiptWayCountry({
          status: 1,
          countryInfoId: item.countryInfoId
        })
          .then(res => {
            console.log(res);
            if (res.code == 200) {
              currentCountryPm = res.data.length > 0 ? true : false;
            }
          })
          .catch(res => {
            console.log(res);
          });
      }

      break;
    case "orderPay":
      if (user.ifPayPassword === "1") {
        fundPss = true;
      } else {
        fundPss = false;
      }
      break;
    case "payment":
      console.log("目前没有支付订单的功能");
      break;
    default:
      break;
  }
  let obj = {
    fund: fundPss,
    idAuth: idAuth
  };
  if (item.type === "withdraw") {
    obj["bind"] = securityBindingNum >= 2 ? true : false;
  }
  if (item.type == "trade") {
    obj["countryPm"] = currentCountryPm;
  }
  return obj;
};

/**
 * 复制
 * @param {*} str 复制的内容
 */
const copyFun = (str, vm) => {
  let text = str + "";
  let input = document.createElement("input");
  let id = "kms" + new Date().toString();
  input.id = id;
  input.style.position = "absolute";
  input.style.zIndex = "-10000";
  input.style.top = "0px";
  input.value = text; // 修改文本框的内容
  document.body.appendChild(input);
  input.select(); // 选中文本
  document.execCommand("copy"); //使文档处于可编辑状态，否则无效
  vm.$Notification.success("", vm.$t("infoAll.copy_succeed"), 2000);
  document.body.removeChild(document.getElementById(id));
};

// 补齐格式不满10加0
function addZero(n) {
  return n < 10 ? "0" + n : n;
}
// 获取剩余时间  传入 '2020-01-01 00:00:00'
const getResidueTime = end => {
  let nowtime = new Date().getTime(); // 当前时间 毫秒数
  let endTime = Date.parse(new Date(end.replace(/-/g, "/"))); //结束时间  毫秒数
  let totalSeconds = (endTime - nowtime) / 1000; // 结束时间-当前时间 = 剩余多少时间
  let day = parseInt(totalSeconds / 3600 / 24); //天
  let hour = parseInt((totalSeconds / 3600) % 24); //时
  let minute = parseInt((totalSeconds / 60) % 60); //分
  let second = parseInt(totalSeconds % 60); //秒
  let residueTime =
    "倒计时：" + day + "天 " + hour + "时 " + minute + "分 " + second + "秒";
  hour = addZero(hour);
  minute = addZero(minute);
  second = addZero(second);
  if (totalSeconds < 0) {
    residueTime = "时间到";
  }
  return residueTime;
};

/**
 * 格式化时间 传入 345546546734
 */
function formate(time) {
  if (time >= 10) {
    return time;
  } else {
    return `0${time}`;
  }
}
const fortmatTime = (leftTime, length) => {
  if (leftTime == undefined) {
    leftTime = 0;
  }
  // let d = formate(parseInt(leftTime / (24 * 60 * 60)));
  let h = formate(parseInt((leftTime / (60 * 60)) % 24));
  let m = formate(parseInt((leftTime / 60) % 60));
  let s = formate(parseInt(leftTime % 60));
  if (leftTime <= 0) {
    // vm.$emit('time-end')
    return "00：00：00";
  }
  if (length) {
    return `${m}:${s}`;
  }
  // return `${d}:${h}:${m}:${s}`;
  return `${h}:${m}:${s}`;
};

/**
 * 判断数据类型
 * @param {*} data
 */
const type = s =>
  Object.prototype.toString
    .call(s)
    .slice(8, -1)
    .toLowerCase();
[
  "Array",
  "Undefined",
  "Null",
  "Boolean",
  "Number",
  "Function",
  "String",
  "Symbol",
  "Object"
].forEach(v => (type["is" + v] = s => type(s) === v.toLowerCase()));

export {
  ICON_MAP,
  throttle,
  debounce,
  debounceFinal,
  throttle2,
  throttle3,
  leapYear,
  pad,
  catIn,
  resetTokenAndClearUser,
  EventBus,
  supportsWebp,
  convertImage,
  supportsWebpInit,
  checkPermissions,
  copyFun,
  getResidueTime,
  fortmatTime,
  type
};
