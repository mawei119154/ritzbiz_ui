import request from "@/utils/request";
import request2 from "@/utils/request2";

/**
 * 发布广告 获取手续费说明
 */
export const queryOtcMarketFeeRate = () =>
  request({
    url: "manage/market/queryOtcMarketFeeRate",
    method: "get"
  });

/**
 * 发布广告
 * @param {*} data.isBuy // 0买入 1卖出
 * @param {*} data.payWayType // 支付方式
 * @param {*} data.coinTypeId // 虚拟货币
 * @param {*} data.countryInfoId // 国家
 * @param {*} data.tradeCoinType // 法定货币 cny 之类，并不是id
 * @param {*} data.otcTradeCurrencyId // 法定货币 id
 * @param {*} data.isAutoTaken // 默认是 "1"
 * @param {*} data.merchantCoinId // 默认 "0"
 * @param {*} data.limitQty // 总限额
 * @param {*} data.maxQty // 最大限额
 * @param {*} data.minQty // 最小限额
 * @param {*} data.pricingType // 价格类型
 * @param {*} data.price // 价格
 * @param {*} data.profitRate // 价格比例
 * @param {*} data.message // 备注
 *
 * payWayType == '0'
 * @param {*} data.receiptWayIds // 收款方式 id
 * @param {*} data.receiptWayTypes // 现金支付的 类型 id
 *
 * payWayType == '1'
 * @param {*} data.giftCardId // 礼品卡 id
 *
 *
 */
export const publishMarketFun = data =>
  request2({
    url: "manage/market/publishMarket",
    method: "post",
    data
  });

/**
 * 编辑广告
 * @param {*} data.id // 广告id
 * @param {*} data.isBuy // 0买入 1卖出
 * @param {*} data.payWayType // 支付方式
 * @param {*} data.coinTypeId // 虚拟货币
 * @param {*} data.countryInfoId // 国家
 * @param {*} data.tradeCoinType // 法定货币 cny 这种
 * @param {*} data.otcTradeCurrencyId // 法定货币  id
 * @param {*} data.isAutoTaken // 默认是 "1"
 * @param {*} data.merchantCoinId // 默认 "0"
 * @param {*} data.limitQty // 总限额
 * @param {*} data.maxQty // 最大限额
 * @param {*} data.minQty // 最小限额
 * @param {*} data.pricingType // 价格类型 0 固定单价 1 浮动单价
 * @param {*} data.price // 价格
 * @param {*} data.profitRate // 价格比例
 * @param {*} data.message // 备注
 *
 * payWayType == '0'
 * @param {*} data.receiptWayIds // 收款方式 id
 * @param {*} data.receiptWayTypes // 现金支付的 类型 id
 *
 * payWayType == '1'
 * @param {*} data.giftCardId // 礼品卡 id
 *
 *
 */
export const editMarketFun = data =>
  request2({
    url: "manage/market/editMarket",
    method: "post",
    data
  });

/**
 * 获取发布广告的基本信息
 * @param {*} data.coinTypeId
 * @param {*} data.countryInfoId
 */
export const getPublishMarketParamFun = data =>
  request2({
    url: "manage/market/getPublishMarketParam",
    method: "post",
    data
  });

/**
 * 获取编辑广告的详情
 * @param {*} data.adId
 */
export const getMarketInfoFun = data =>
  request2({
    url: "manage/market/getMarketInfo",
    method: "post",
    data
  });

/**
 * 获取市场价格
 * @param {*} data.coinTypeId // 虚拟货币的id
 * @param {*} data.otcTradeCurrency // cny usd sgd 等
 */
export const getMarketPrice = data =>
  request({
    url: "manage/market/getPrice",
    method: "get",
    params: data
  });
