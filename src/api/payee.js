import request from "@/utils/request";
import request2 from "@/utils/request2";

// id
export const payeeDelete = data =>
  request2({
    url: "manage/payee/delete",
    method: "post",
    data
  });

export const getPayeeList = data =>
  request({
    url: "manage/payee/list",
    method: "get",
    params: data
  });
// id
export const getPayeeInfo = data =>
  request({
    url: "manage/payee/info/%7Bid%7D",
    method: "get",
    params: data
  });

export const payeeUpdate = data =>
  request({
    url: "manage/payee/update",
    method: "post",
    data
  });
export const payeeSave = data =>
  request({
    url: "manage/payee/save",
    method: "post",
    data
  });
