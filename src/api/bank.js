import request from "@/utils/request";

export const getBankList = () =>
  request({
    url: "manage/bankcard/list",
    method: "get"
  });
// cardNo
export const getBankInfoCardNo = data =>
  request({
    url: "manage/bankcard/info/%7BcardNo%7D",
    method: "get",
    params: data
  });
// id
export const getBankInfoId = data =>
  request({
    url: "manage/bankcard/info/%7Bid%7D",
    method: "get",
    params: data
  });
