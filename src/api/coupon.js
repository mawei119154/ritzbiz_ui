import request from "@/utils/request";

//
export const getCouponList = data =>
  request({
    url: "manage/coupon/list",
    method: "post",
    data
  });
// useTag 优惠券状态：0可使用，1已使用，已过期  page rows
export const getCouponPageList = data =>
  request({
    url: "manage/coupon/pagelist",
    method: "post",
    data
  });
