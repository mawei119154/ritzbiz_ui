import request from "@/utils/request";
import request2 from "@/utils/request2";
/**
 * role:   0：普通用户  1：机构用户（商家） 2：商户(币商） 3：中间支付账号 4：虚拟币入金支付账号 5：商户代理用户
 *          isMerchant=1 && role=1的是商户---并非2商户(币商)
 *
 */

/**
 * 机构 role == 2 没有权限 有 支付订单功能页面
 *
 * 获取支付订单 列表
 * @param {*} data.rows
 * @param {*} data.page
 * @param {*} data.startTime
 * @param {*} data.endTime
 * @param {*} data.content // 订单号，用户名，真实姓名
 *
 * （商户）账号  isMerchant == 1    中间账号 role == 3  虚拟币入金支付账号 role == 4
 * @param {*} data.timeTag // 时间状态
 * @param {*} data.status // 状态
 * @param {*} data.coinTypeId
 *
 * 代理 账号  role == 5
 * @param {*} data.timeTag // 时间状态
 * @param {*} data.status // 状态
 * @param {*} data.coinTypeId
 * @param {*} data.merchantId // 所选商户的 id ；可以 234535，34534，3453453 这样的字符串表示
 */
export const getPayOrderList = data =>
  request2({
    url: "manage/payOrder/pageList",
    method: "post",
    data
  });

/**
 * 获取 在 代理用户模式下 的 商户列表
 */
export const getAgentMerchantList = () =>
  request2({
    url: "manage/payOrder/queryAgentMerchantList",
    method: "post"
  });

/**
 * 导出支付记录
 * @param {*} item
 */
export const exportPayOrderExcel = data => {
  return request({
    url: "manage/payOrder/getExcel",
    method: "post",
    data,
    responseType: "blob"
  });
};
/**
 * 确认支付
 * @param {*} data.id 订单号
 * @param {*} data.payPassword 资金密码
 */
export const payOrderConfirmFun = data =>
  request2({
    url: "manage/payOrder/confirm",
    method: "post",
    data
  });

/**
 * 获取未支付的订单个数
 */
export const unPayNumFun = () =>
  request2({
    url: "manage/payOrder/unPayNum",
    method: "post"
  });
