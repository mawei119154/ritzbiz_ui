import request from "@/utils/request";
import request2 from "@/utils/request2";

/**
 * 获取支付方式列表
 */
export const getAssetsFun = () =>
  request({
    url: "manage/assets/getAssets",
    method: "post"
  });

/**
 * 获取总资产
 */
export const getAssetsTotalFun = () =>
  request({
    url: "manage/assets/getAssetsConvertUsdtAmount",
    method: "post"
  });

/**
 * 获取当前币种的充币地址
 * @param {*} data
 * @param {*} data.coinTypeId // coinTypeId != 2 -> coinTypeId=coinTypeId | coinTypeId ==2 -> usdt -> chain == OMNI -> coinTypeId=coinTypeId -> chain == ERC20 -> coinTypeId=3
 */
export const getdeposityAddressFun = data =>
  request2({
    url: "manage/wallet/getWalletDeposityAddress",
    method: "post",
    data
  });
/**
 * 获取当前币种的提币地址
 * @param {*} data
 * @param {*} data.coinTypeId //coinTypeId=coinTypeId 不变
 * @param return [ {id:121,extractAddress:'ggdfgdgsfd'}]
 */
export const queryExtractAddressFun = data =>
  request2({
    url: "manage/assetsExtractAddress/queryExtractAddress",
    method: "post",
    data
  });
/**
 * 获取当前币种的限制条件
 * coinTypeId
 * @param {*} data
 * @param {*} data.coinTypeId // coinTypeId != 2 -> coinTypeId=coinTypeId | coinTypeId ==2 -> usdt -> chain == OMNI -> coinTypeId=coinTypeId -> chain == ERC20 -> coinTypeId=3
 */
export const getExtractLimitFun = data =>
  request2({
    url: "manage/assetsExtractOrder/getExtractAndDepositLimit",
    method: "post",
    data
  });
/**
 * 获取屏幕上滚动的  币种 的当前 价格
 * @param {*} data
 */
export const getTickerFun = () =>
  request({
    url: "manage/assets/ticker",
    method: "post"
  });
//-----------------------
/**
 * 提币计算手续费
 * @param {*} data
 * @param {*} data.amount
 * @param {*} data.coinTypeId
 */
export const getCalcHandingFee = data =>
  request2({
    url: "manage/assetsExtractOrder/getCalcHandingFee",
    method: "post",
    data
  });
/**
 * 检查资金验证码是否存在/ 感觉这个方法可以不需要了，因为在点击触发提币的时候已经做了验证
 * @param {*} data
 * @param {*} data.payPassword
 */
export const checkPayPasswordFun = data =>
  request2({
    url: "manage/user/checkPayPassword",
    method: "post",
    data
  });
/**
 * 提币操作
 * @param {*} data
 * @param {*} data.receiveAddress
 * @param {*} data.amount
 * @param {*} data.coinTypeId
 * @param {*} data.type // coinTypeId== 1 -> btc -> no type;|| coinTypeId== 4 -> eth -> type = 1; coinTypeId == 2 -> usdt -> OMNI -> type = 0 , ERC20 -> type =1;
 * @param {*} data.emailCode
 * @param {*} data.phoneCode
 * @param {*} data.googleCode
 */
export const extractFun = data =>
  request2({
    url: "manage/assetsExtractOrder/extract",
    method: "post",
    data
  });

/**
 * 验证提币地址 是不是有问题
 * @param {*} data.coinTypeId
 * @param {*} data.receiveAddress
 */
export const checkAddressFun = data =>
  request2({
    url: "manage/assetsExtractOrder/checkWithdrawAddress",
    method: "post",
    data
  });

/**
 *  划转时 账户和用户名相互查询
 * @param {*} data
 * @param {*} data.username
 * @param {*} data.accountNumber 选其一
 */
export const getUserInfoFun = data =>
  request2({
    url: "manage/user/getUserInfo",
    method: "post",
    data
  });

/**
 * 获取划转手续费
 * @param {*} data.amount
 * @param {*} data.coinTypeId
 * @param {*} data.toUsername
 */
export const getUserTransferFeeFun = data =>
  request2({
    url: "manage/transferInner/handingFee",
    method: "post",
    data
  });

/**
 * 划转操作
 * @param {*} data.amount
 * @param {*} data.coinTypeId
 * @param {*} data.payPassword
 * @param {*} data.toAccount
 * @param {*} data.toUsername
 */
export const transferInnerFun = data =>
  request2({
    url: "manage/transferInner/transfer",
    method: "post",
    data
  });

/**
 * 获取 资产操作记录
 * @param {*} data.page
 * @param {*} data.rows
 * @param {*} data.queryType // 0 充币 1 提币
 * @param {*} data.coinTypeId // 币种
 */
export const getExtractAndDepositLimit = data =>
  request2({
    url: "manage/wallet/getWithdrawOrDepositList",
    method: "post",
    data
  });

/**
 * 获取所有支持交易的币种
 * @param {*} data
 */
export const getCoinsFun = () =>
  request2({
    url: "manage/coin/coins",
    method: "post"
  });

/**
 * 获取转入 转出 记录
 * @param {*} data.page
 * @param {*} data.rows
 * @param {*} data.tag //  0 内部转入 1 内部转出
 * @param {*} data.coinTypeId // 币种
 */
export const getTransferList = data =>
  request2({
    url: "manage/transferInner/pageList",
    method: "post",
    data
  });

/**
 * 导出充币记录
 * @param {*} item
 */
export const exportDepositExcel = item => {
  return request({
    url: "manage/wallet/exportDepositExcel",
    method: "post",
    item,
    responseType: "blob"
  });
};
/**
 * 导出提币记录
 * @param {*} item
 */
export const exportWithdrawExcel = item => {
  return request({
    url: "manage/wallet/exportWithdrawExcel",
    method: "post",
    item,
    responseType: "blob"
  });
};
/**
 * 导出划转记录
 * @param {*} item
 */
export const exportTransferInnerExcel = data => {
  return request({
    url: "manage/transferInner/getExcel",
    method: "post",
    data,
    responseType: "blob"
  });
};
export function downloadByAElement(url) {
  try {
    const element = document.createElement("a");
    console.log(element);
    element.setAttribute("id", "gete");
    element.href = url;
    // element.download = fileName;
    const a = document.body.appendChild(element);
    a.click();
    document.body.removeChild("#gete");
  } catch (e) {
    console.log(e);
  }
}

/**
 * 获取提币地址列表
 * @param {*} data.coinTypeId
 * @param {*} data.rows
 * @param {*} data.page
 */
export const getAddressList = data =>
  request2({
    url: "manage/assetsExtractAddress/pageList",
    method: "post",
    data
  });
/**
 * 增加提币地址
 * @param {*} data.address // address
 * @param {*} data.type // chain coinTypeId==4 时 => type = 1,  coinTypeID == 3 type 不存在，
 * @param {*} data.coinTypeId // 币种
 * @param {*} data.remark // 备注
 * @param {*} data.emailCode
 * @param {*} data.phoneCode
 * @param {*} data.googleCode
 */
export const addExtractAddressFun = data =>
  request2({
    url: "manage/assetsExtractAddress/add",
    method: "post",
    data
  });
/**
 * 删除提币地址
 * @param {*} data.id
 */
export const deleteExtractAddressFun = data =>
  request2({
    url: "manage/assetsExtractAddress/delete",
    method: "post",
    data
  });
