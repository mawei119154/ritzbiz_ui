import request2 from "@/utils/request2";

/**
 * 首页输入价格计算
 * @param {*} data.amount // 数量
 * @param {*} data.inputCoinType 输入的是 哪一种 0 法币 1 虚拟货币
 * @param {*} data.legalTenderCoinName // 法币
 * @param {*} data.tradeCoinName // 虚拟货币
 *
 * @returns {
 *  legalTenderCoinAmount 法币
 *  tradeCoinAmount 虚拟货币
 * }
 */
export const priceCalc = data =>
  request2({
    url: "manage/index/priceCalc",
    method: "post",
    data
  });

/**
 * 获取首页虚拟货币
 * @param {*} data
 */
export const queryTradeCoin = () =>
  request2({
    url: "manage/index/queryTradeCoin",
    method: "get"
  });

/**
 * 获取 首页法币列表
 */
export const queryLegalTradeCoin = () =>
  request2({
    url: "manage/index/queryLegalTenderCoin",
    method: "get"
  });
