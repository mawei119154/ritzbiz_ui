import request from "@/utils/request";
import request2 from "@/utils/request2";

/**
 * 获取币商的详情
 * @param {*} data.userId
 */
export const getMerchantInfo = data => {
  return request({
    url: "manage/market/userMarketDetail",
    method: "get",
    params: data
  });
};

/**
 * 获取币商的广告列表
 * @param {*} data.isBuy
 * @param {*} data.userId
 */
export const merchantAds = data => {
  return request2({
    url: "manage/market/userMarketList",
    method: "post",
    data
  });
};
