import request from "@/utils/request";
// import request2 from "@/utils/request2";

/**
 * 获取聊天记录
 * @param {*} data.room_id //房间号
 * @param {*} data.page // 页数
 * @param {*} data.size // 每页大小
 * @param {*} data.unread // 是否未读 0
 * @param {*} data.reversed // 获取记录 0 正常获取  倒叙 1
 */
export const getMessageList = data =>
  request({
    url: "websocket/chat/room/message/list",
    method: "get",
    params: data
  });

/**
 * 标记消息是否已读
 * @param {*} data.room_id // 房间号
 * @param {*} data.msg_ids // 消息id
 */
export const markMessageRead = data =>
  request({
    url: "websocket/chat/room/message/mark_read",
    method: "post",
    data
  });

/**
 * 获取socket 通信地址
 */
export const getSocketUrl = () =>
  request({
    url: "manage/constant/chat",
    method: "get"
  });

/**
 * 登录socket
 * @param {*} data.token // 用户token
 */
export const socketLogin = data =>
  request({
    url: "websocket/user/login",
    method: "post",
    data
  });
