import request from "@/utils/request";
import request2 from "@/utils/request2";

/**
 * 获取国家列表
 */
export const getCountryList = () =>
  request({
    url: "manage/market/queryCountryList",
    method: "get"
  });
/**
 * 获取礼品卡
 * @param {*} data
 */
export const getGiftCardFun = () =>
  request({
    url: "manage/market/queryGiftCard",
    method: "post"
  });
/**
 * 根据国际id（countryInfoId）查出其国家支持的法币
 * @param {*} data.countryInfoId
 */
export const queryOtcMarketCurrency = data =>
  request2({
    url: "manage/market/queryOtcMarketCurrency",
    method: "get",
    params: data
  });
// remittanceTag 0 查询开户国家，其他不传
export const getCountryCurrencyList = data =>
  request({
    url: "manage/country/list",
    method: "get",
    params: data
  });
export const getCountryCurrencyInfo = data =>
  request({
    url: "manage/country/info/%7Bid%7D",
    method: "get",
    params: data
  });
