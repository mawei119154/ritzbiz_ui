// import request from "@/utils/request";
import request2 from "@/utils/request2";

/**
 * 预检测账号是否存在
 * @param {*} data 
 *  type: email
    email: 1191546352@qq.com
    emailCode: 123213

    type: phone
    phone: +86,1231231
    phoneCode: 12341

    type: google
    googleCode: sdfsd
 */
export const prebindingFun = data =>
  request2({
    url: "manage/user/prebinding",
    method: "post",
    data
  });

/**
   * accountCode:this.email_phone_code_two,
     emailCode:this.email_code,
     email:this.email,
   * @param {*} data 
   */
export const binEmailFun = data =>
  request2({
    url: "manage/user/bindingEmail",
    method: "post",
    data
  });
/**
   * accountCode:this.email_phone_code_two,
     emailCode:this.email_code,
     email:this.email,
   * @param {*} data 
   */
export const binPhoneFun = data =>
  request2({
    url: "manage/user/bindingPhone",
    method: "post",
    data
  });
/**
   * code:this.email_phone_code_two,
     googleCode:this.google_verification,
   * @param {*} data 
   */
export const binGoogleFun = data =>
  request2({
    url: "manage/user/bindingGoogleAuth",
    method: "post",
    data
  });
/**
   * code: sdsfds 验证码
      oldGoogleCode: sdfs
      newGoogleCode: sdfs
   * @param {*} data 
   */
export const changeGoogleBinFun = data =>
  request2({
    url: "manage/user/changeGoogleAuth",
    method: "post",
    data
  });
/**
 * 获取谷歌验证的二维码和 密钥
 * @param {*} data
 */
export const getGoogleAuthFun = data =>
  request2({
    url: "manage/user/getGoogleAuthInfo",
    method: "post",
    data
  });

/**
 * 身份认证
 *  credentialsNumber:this.id_number, 证件号码
    credentialsPhotoBack:this.dialogImageUrlKey, 证件背面 1 时 没有这个参数
    credentialsPhotoFront:this.dialogImageUrlHeadKey, 证件正面
    credentialsPhotoHandheld:this.dialogImageUrlManKey, 手持证件
    credentialsType:this.certificate, 证件类型 0 身份证 1 护照
    name:this.true_name, 姓名
    nationality:this.county, 国家
 * @param {*} data 
 */
export const addIdentityAuthInfoFun = data =>
  request2({
    url: "manage/user/addIdentityAuthInfo",
    method: "post",
    data
  });
