// import request from "@/utils/request";
import request2 from "@/utils/request2";

/**
 * 获取广告列表
 * @param {*} data.rows
 * @param {*} data.page
 * @param {*} data.countryInfoId
 * @param {*} data.adStatus // 广告状态 2 已下架 1 已上架  0 下架中
 * @param {*} data.isBuy 交易类型 0 购买  1 出售
 * @param {*} data.otcTradeCurrencyId 法币类型（id）
 * @param {*} data.coinTypeId 虚拟货币类型（id）
 * @param {*} data.id 搜索 广告id
 * @param {*} data.payWayType 支付方式
 * @param {*} data.startTime
 * @param {*} data.endTime
 */
export const queryAdsList = data =>
  request2({
    url: "manage/market/queryUserMarketList",
    method: "post",
    data
  });
/**
 * 下架广告
 * @param {*} data.adId // 广告id
 */
export const unMarketFun = data =>
  request2({
    url: "manage/market/unMarket",
    method: "post",
    data
  });

/**
 * 删除广告
 * @param {*} data.adId // 广告id
 */
export const deleteMarketFun = data =>
  request2({
    url: "manage/market/deleteMarket",
    method: "post",
    data
  });
/**
 * 上架广告
 * @param {*} data.adId
 */
export const rePublishMarketFun = data =>
  request2({
    url: "manage/market/rePublishMarket",
    method: "post",
    data
  });
/**
 * 取消预约
 * @param {*} data.adId
 */
export const cancleUnPublishMarketFun = data =>
  request2({
    url: "manage/market/cancleUnPublishMarket",
    method: "post",
    data
  });
