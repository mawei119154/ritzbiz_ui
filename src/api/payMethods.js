import request from "@/utils/request";
import request2 from "@/utils/request2";

/**
 * 获取支付方式列表
 */
export const getPayMethods = () =>
  request({
    url: "manage/receiptWay/list",
    method: "post"
  });
/**
 * 修改支付方式的状态
 * @param {*} data
 */
export const changePayMethods = data =>
  request({
    url: "manage/receiptWay/activation",
    method: "post",
    params: data //{id:, status:}
  });
/**
 *
 * 删除选定的支付防止
 * @param {*} data
 */
export const deletePayMethods = data =>
  request({
    url: "manage/receiptWay/delete",
    method: "post",
    params: data // id
  });

/**
 * 新增支付方式
 * @param {*} data
 */
export const addPayMethods = data =>
  request({
    url: "manage/receiptWay/save",
    method: "post",
    params: data
    /**
     *  name: %E6%B5%8B%E8%AF%95%E4%B8%AAhi
        bankAccount: %E6%98%AF%E8%B1%86%E8%85%90%E5%B9%B2%E5%A3%AB%E5%A4%A7%E5%A4%AB
        bankCardNumber: 12342534654
        branchAccount: %E5%A3%AB%E5%A4%A7%E5%A4%AB%E6%95%A2%E6%AD%BB%E9%98%9F%E9%A3%8E%E6%A0%BC
        payPassword: zhima123
        type: 0
        countryInfoId: 1

        name: ceshi
        alipayAccount: dfgdfgd
        wxAccount: werw
        payPassword: zhima123
        qrCode: /identity/d6e5015e1e284726827548f3b5e2f411.jpg
        type: 1
        countryInfoId: 1
     */
  });

/**
 * 获取当前国家的收款方式
 * @param {*} data
 * @param {*} data.status // 状态 1 激活
 * @param {*} data.countryInfoId // required 国家的id
 */
export const getReceiptWayCountry = data => {
  // console.log(data);
  return request2({
    url: "manage/receiptWay/list",
    method: "post",
    data
  });
};
/**
 * 获取国家列表
 */
export const getMarketCountry = () =>
  request({
    url: "manage/market/queryCountryList",
    method: "get"
  });
