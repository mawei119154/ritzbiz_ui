import request from "@/utils/request";

// bizType 1 收入来源  2 汇款用途  3 收款人关系  4 职业
export const getDictList = data =>
  request({
    url: "manage/dict/list/%7BbizType%7D",
    method: "get",
    params: data
  });
// id
export const getDictInfo = data =>
  request({
    url: "manage/dict/info/%7Bid%7D",
    method: "get",
    params: data
  });
