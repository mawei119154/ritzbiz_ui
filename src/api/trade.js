import request from "@/utils/request";
import request2 from "@/utils/request2";

/**
 * 获取法币广告列表
 * @param {*} data.coinTypeId
 * @param {*} data.isBuy: "0", // 0 in   1 out
 * @param {*} data.coinTypeId: "",
 * @param {*} data.countryInfoId: "",
 * @param {*} data.payWayType: "0", // 0 现金 1 礼品卡
 * @param {*} data.giftCardId: "", // 礼品卡id
 * @param {*} data.receiptWayTypes: "" // 现金 id
 */
export const queryAllMarketList = data =>
  request2({
    url: "manage/market/queryAllMarketList",
    method: "post",
    data
  });

export const queryTopMarketList = data =>
  request2({
    url: "manage/market/queryTopMarketList",
    method: "post",
    data
  });

/**
 * 进行中订单个数查询
 */
export const orderTradeWait = () =>
  request2({
    url: "manage/market/getOtcMarketOrderCount",
    method: "post"
  });

/**
 * 卖出广告
 * @param {*} data.amount
 * @param {*} data.otcAdvertisementId
 * @param {*} data.payPassword
 * @param {*} data.tag // 1
 * @param {*} data.price
 */
export const cnyOrderSell = data =>
  request2({
    url: "manage/cnyOrder/sell",
    method: "post",
    data
  });
/**
 * 买入广告
 * @param {*} data.amount
 * @param {*} data.otcAdvertisementId
 * @param {*} data.tag // 1
 * @param {*} data.price
 */
export const cnyOrderBuy = data =>
  request2({
    url: "manage/cnyOrder/buy",
    method: "post",
    data
  });

/**
 * 买入/卖出 计算
 * @param {*} data.inputCurrencyPrice 价格
 * @param {*} data.inputRmbAmount 输入的是真实货币量 1
 * @param {*} data.inputCurrencyAmount 输入的是虚拟货币的量 2
 * inputRmbAmount inputCurrencyAmount 存其一
 * @returns {
 *    rmbAmount: "", // 真实货币的量
 *    tradeCurrencyAmount: "" // 虚拟货币的量
 * }
 */
export const getCurrencyPriceCalc = data =>
  request2({
    url: "manage/market/getCurrencyPriceCalc",
    method: "post",
    data
  });

/**
 * 获取trade order list
 * @param {*} data.rows
 * @param {*} data.page
 * @param {*} data.countryInfoId
 * @param {*} data.status 状态  0 待支付 1 已支付 2 完成 3 关闭
 * @param {*} data.transactionType 交易类型 0 购买  1 出售
 * @param {*} data.otcTradeCurrencyId  法币类型（id）
 * @param {*} data.coinTypeId  虚拟货币类型（id）
 * @param {*} data.id  搜索 订单号/交易对象查询
 * @param {*} data.startTime  时间
 * @param {*} data.endTime  时间
 * 
 * @returns {
 *  amount: "0.28571429"
    amountCny: "2"
    cnyUnitPrice: "7"
    coinType: "usdt"
    coinTypeId: "2"
    complaintRole: null
    complaintStatus: null
    id: "15923003319330058"
    isComplaint: "0"
    oppositeUserId: "1201"
    optStatus: "1"
    role: "0"
    startTime: "2020-06-16 17:38:52"
    status: "3"
    tRealName: "郑德兴"
    tUsername: "USDT在线秒放"
    tradeCoinType: "CNY"
    transactionType: "0"
    transactionTypeOfUser: "0"
 * }
 */
export const cnyOrderList = data =>
  request2({
    url: "manage/cnyOrder/pageList",
    method: "post",
    data
  });
/**
 * 导出我的订单（交易订单）
 * @param {*} item
 */
export const exportTradeOrderExcel = data => {
  return request({
    url: "manage/cnyOrder/getExcel",
    method: "post",
    data,
    responseType: "blob"
  });
};

/**
 * 查询交易订单详情
 * @param {*} data.id
 */
export const tradeOrderFind = data =>
  request2({
    url: "manage/cnyOrder/findById",
    method: "post",
    data
  });
/**
 * 顶到对应的收款方式列表
 * @param {*} data.id
 */
export const adsReceiptWaysFun = data =>
  request2({
    url: "manage/cnyOrder/receiptWays",
    method: "post",
    data
  });

/**
 * 标记已付款
 * @param {*} data.id
 * @param {*} data.receiptWayId // 收款方式
 */
export const confirmMarkFun = data =>
  request2({
    url: "manage/cnyOrder/confirmPay",
    method: "post",
    data
  });

/**
 * 取消交易订单
 * @param {*} data.id
 */
export const cancleTradeOrderFun = data =>
  request2({
    url: "manage/cnyOrder/cancel",
    method: "post",
    data
  });

/**
 * 提交申诉原因
 * @param {*} data.cnyTransactionOrderId
 * @param {*} data.description
 */
export const tradeComplaintOrderFun = data =>
  request2({
    url: "manage/cnyOrder/complaint",
    method: "post",
    data
  });

/**
 * 放行订单
 * @param {*} data.id
 */
export const releaseOrderFun = data =>
  request2({
    url: "manage/cnyOrder/release",
    method: "post",
    data
  });
