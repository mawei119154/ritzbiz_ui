import request from "@/utils/request";

// export const getBankList = (data) => request({
//     url: 'manage/bankcard/list',
//     method: 'get',
// })
// // cardNo
// export const getBankInfoCardNo = (data) => request({
//     url: 'manage/bankcard/info/%7BcardNo%7D',
//     method: 'get',
//     params: data
// })
//
/**
 * 
 * @param { "amount": 10000,
  "currencyCode": "USD",
  "incomeSources": "工资",
  "purpose": "职工报酬",
  "tcouponId": 0,//优惠券
  "userPayeeId": 1} data 
 */
export const createOrder = data =>
  request({
    url: "manage/order/save",
    method: "post",
    data
  });
