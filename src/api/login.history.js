import request from "@/utils/request";
// import request2 from "@/utils/request2";

/**
 * 获取支付方式列表
 */
export const getLoginHistory = () =>
  request({
    url: "manage/user/getLoginLog",
    method: "post"
  });
