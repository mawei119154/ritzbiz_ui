import request from "@/utils/request";

export const getIdentityList = () =>
  request({
    url: "manage/identity/list",
    method: "get"
  });
export const getIdentityInfo = data =>
  request({
    url: "manage/identity/info/%7Bid%7D",
    method: "get",
    params: data
  });
/**
 *
 * @param {accountCountryId} 开户国家 int
 * @param {birthDate} 生日 string
 * @param {cnName} 中文名
 * @param {cnSurname} 中文姓
 * @param {countryId} 国籍编号
 * @param {email} 邮箱
 * @param {identityAuthOneImg} 证件图片1
 * @param {identityAuthTwoImg} 证件图片2
 * @param {identityType} 证件类型：0护照，1驾照
 * @param {incomeSources} 收入来源，直接传中文(查字典，bitType=1)
 * @param {job} 职业，直接传中文(查字典，bitType=4)
 * @param {residentialAddress} 居住地址
 * @param {residentialAddressImg} 地址证明图片
 * @param {sex} 性别：0男，1女
 * @param {spellName} 拼音名
 * @param {spellSurname} 拼音姓
 */
export const identitySave = data =>
  request({
    url: "manage/identity/save",
    method: "post",
    data
  });
/** ------------------------------ */

/**
 * 获取用户信息
 * @param {*} data
 */
export const getOwnInfoFun = () =>
  request({
    url: "manage/user/getOwnInfo",
    method: "post"
  });
export const changePasswordFun = data =>
  request({
    url: "manage/user/changePassword",
    method: "post",
    data
  });
export const setPayPasswordFun = data =>
  request({
    url: "manage/user/setPayPassword",
    method: "post",
    data
  });
