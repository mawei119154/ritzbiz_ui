import request from "@/utils/request";
import request2 from "@/utils/request2";

export const loginByUsername = data =>
  request2({
    url: "manage/login",
    method: "post",
    data
  });
export const logoutByUsername = () =>
  request({
    url: "manage/logout",
    method: "get"
  });
export const registeredByUsername = data =>
  request2({
    url: "manage/registered",
    method: "post",
    data
  });
// behaviorType  0注册，1手机换绑，2找回密码
/**
 * 获取验证码
 * behaviorType 0：注册 1:修改密码 2：忘记密码 3：设置资金密码 4：重置资金密码 5：Google验证 6：添加提币地址 7：提币 8：绑定手机-新手机号验证 9:绑定手机-账号验证 10：支付 11：绑定邮箱
 * @param {*} data
 * @param {*} data.accountType 0 phone 1 email
 * @param {*} data.behaviorType 行为 11 绑定新邮箱 8 更换手机号 5 绑定谷歌验证器
 * @param {*} data.number 手机|邮箱
 */
export const postCode = data => {
  // console.log(data)
  // request2
  return request({
    url: "manage/getCode",
    method: "get",
    // method: "post",
    params: data
    // data
  });
};
// 检查用户名是否可用
export const userNameAvailable = data =>
  request({
    url: "manage/isUserNameAvailable",
    method: "get",
    data
  });
// 检查手机是否可用
export const phoneAvailable = data =>
  request({
    url: "manage/isPhoneAvailable",
    method: "get",
    data
  });
export const resetPassword = data =>
  request2({
    url: "manage/resetPassword",
    method: "post",
    data
  });
export const checkPhone = data =>
  request2({
    url: "manage/checkPhoneCode",
    method: "post",
    data
  });
export const resetPhone = data =>
  request2({
    url: "manage/resetPhone",
    method: "post",
    data
  });
// accountIdentityCheckStatus -1未认证，0审核中，1已审核（通过），2未通过
export const userInfo = data =>
  request({
    url: "manage/user/info",
    method: "get",
    data
  });
export const imgUpload = (data, config) => {
  return request({
    url: "manage/file/upload",
    method: "post",
    data,
    onUploadProgress: config.onUploadProgress
  });
};
