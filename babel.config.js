module.exports = {
  presets: [
    ["@vue/cli-plugin-babel/preset", { useBuiltIns: "entry" }]
    // [
    //   ("@vue/app",
    //   {
    //     polyfills: [
    //       "es6.promise",
    //       "es6.symbol",
    //       "es6.array.iterator",
    //       "es7.promise.finally"
    //     ]
    //   })
    // ]
  ],
  plugins: [
    [
      "import",
      {
        libraryName: "heyui",
        libraryDirectory: "lib/components"
      }
    ]
  ]
};
