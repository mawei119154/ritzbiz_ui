// const { SkeletonPlugin } = require("page-skeleton-webpack-plugin");
// const SkeletonWebpackPlugin = require("vue-skeleton-webpack-plugin");
const path = require("path");
const CompressionWebpackPlugin = require("compression-webpack-plugin");
const productionGzipExtensions = ["js", "css"];
// const WorkboxWebpackPlugin = require("workbox-webpack-plugin");

const utils = {
  assetsPath: function(_path) {
    const assetsSubDirectory =
      process.env.NODE_ENV === "production"
        ? // 生产环境下的 static 路径
          "static"
        : // 开发环境下的 static 路径
          "static";

    return path.posix.join(assetsSubDirectory, _path);
  },
  resolve: function(dir) {
    return path.join(__dirname, "..", dir);
  }
};

module.exports = {
  parallel: true,
  productionSourceMap: false,
  configureWebpack: {
    module: {
      rules: [
        {
          test: /\.(woff2?|eot|ttf|otf)(\?.*)$/,
          loader: "url-loader",
          options: {
            limit: 10000,
            name: utils.assetsPath("fonts/[name].[hash:7].[ext]")
          }
        },
        {
          test: /\.(doc|docx|webm|ogg|wav|flac|aac)(\?.*)?$/,
          loader: "file-loader",
          options: {
            limit: 10000,
            name: "doc/[name].[ext]"
          }
        }
      ]
    },
    plugins: [
      new CompressionWebpackPlugin({
        filename: "[path].gz[query]",
        algorithm: "gzip",
        test: new RegExp("\\.(" + productionGzipExtensions.join("|") + ")$"),
        threshold: 10240,
        minRatio: 0.8
      }),
      require("autoprefixer")
      // new WorkboxWebpackPlugin.GenerateSW({
      //   clientsClaim: true,
      //   skipWaiting: true
      // })
      // new SkeletonPlugin({
      //   pathname: path.resolve(__dirname, "./shell"), // 用来存储 shell 文件的地址
      //   staticDir: path.resolve(__dirname, "./dist"), // 最好和 `output.path` 相同
      //   routes: ["/", "/trade", "/order", "/assets", "/profile"], // 将需要生成骨架屏的路由添加到数组中
      //   headless: false // 打开非headless chrome
      // }),
      // new SkeletonWebpackPlugin({
      //   webpackConfig: {
      //     entry: {
      //       app: path.join(__dirname, "./src/entry-skeleton.js") //这里为上面的entry-skeleton.js
      //     }
      //   },
      //   quiet: true,
      //   router: {
      //     mode: "history",
      //     routes: [
      //       {
      //         path: "",
      //         skeletonId: "skeleton"
      //       },
      //       {
      //         path: "/home", //和router.js中的路径一样就行
      //         skeletonId: "skeleton1" //之前的id
      //       },
      //       {
      //         path: "/test",
      //         skeletonId: "skeleton2"
      //       }
      //     ]
      //   }
      // })
    ],
    optimization: {
      splitChunks: {
        chunks: "all", // 'all' | 'async'异步 | 'initial'分离同步
        // 这表明将选择哪些块进行优化   提供all可能特别强大，因为它意味着即使在异步和非异步块之间也可以共享块。
        // chunks (chunk) {
        //     // exclude `my-excluded-chunk`
        //     return chunk.name !== 'my-excluded-chunk';
        // },
        // 或者，您可以提供更多控制功能。返回值将指示是否包括每个块
        minSize: 30000,
        // 生成块的最小大小（以字节为单位）
        maxSize: 0,
        minChunks: 1,
        // 拆分前必须共享模块的最小块数
        maxAsyncRequests: 15,
        // 按需加载块时并行请求的最大数量将 <= 5
        maxInitialRequests: 13,
        // 初始页面加载时并行请求的最大数量将 <= 3
        automaticNameDelimiter: "~",
        // webpack将使用块的来源和名称生成名称（例如vendors~main.js）。此选项使您可以指定用于生成名称的定界符
        name: true, // true | function (module, chunks, cacheGroupKey) | string
        // 拆分块的名称 提供true将基于块和缓存组密钥自动生成一个名称。提供字符串或函数将允许您使用自定义名称。如果名称与入口点名称匹配，则入口点将被删除。
        // cacheGroups: {
        //     // 缓存组可以继承和/或覆盖splitChunks.*;中的任何选项
        //     // 但是test，priority并且reuseExistingChunk只能在高速缓存组级别配置。要禁用任何默认缓存组，请将它们设置为false
        //     // default: false,
        //     react: {
        //         // reuseExistingChunk: true, // boolean
        //         // 如果当前块包含已从主捆绑包中拆分出的模块，则将重用该模块，而不是生成新的模块。这可能会影响块的结果文件名。
        //         // test(module,chunks) { // function (module, chunk) | RegExp | string
        //         //     // 控制此缓存组选择的模块。省略它会选择所有模块。它可以匹配绝对模块资源路径或块名称。匹配块名称时，将选择块中的所有模块
        //         //     // ...
        //         //     return module.type === 'javascript/auto'
        //         // },
        //         test: /[\\/]node_modules[\\/](react)[\\/]/,
        //         filename: 'js/[name].js',
        //         // enforce: true, // 忽略全局配置
        //         // 让webpack忽略 splitChunks.minSize|minChunks|maxAsyncRequests|maxInitialRequests 选项 and always create chunks for this cache group
        //         // chunks: 'all',
        //         // priority:10, //优先级   当重复的时候 使用优先级高的

        //     },
        //     react_dom: {
        //         test: /[\\/]node_modules[\\/](react-dom)[\\/]/,
        //         filename: 'js/[name].js',
        //         // enforce: true,
        //     },
        //     lodash: {
        //         test: /[\\/]node_modules[\\/](lodash)[\\/]/,
        //         filename: 'js/[name].js',
        //         // enforce: true,
        //     }
        // }
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name(module) {
              // get the name. E.g. node_modules/packageName/not/this/part.js
              // or node_modules/packageName
              const packageName = module.context.match(
                /[\\/]node_modules[\\/](.*?)([\\/]|$)/
              )[1];

              // npm package names are URL-safe, but some servers don't like @ symbols
              return `lib/npm.${packageName.replace("@", "")}`;
            }
          },
          common: {
            //公共模块
            chunks: "initial",
            name: "common",
            minSize: 100, //大小超过100个字节
            minChunks: 3 //最少引入了3次
          }
        }
      }
    }
  },
  css: {
    // 打开开发阶段有骨架屏样式，但是所有样式无法热加载；关闭开发阶段无法加载骨架屏样式 SkeletonWebpackPlugin
    // extract: true
    loaderOptions: {
      postcss: {
        plugins: [
          require("autoprefixer")({
            browsers: ["Android >= 4.0", "iOS >= 7"]
          }),
          require("postcss-pxtorem")({
            rootValue: 192, // 基准font-size 16  -特殊换算的基数(设计图750的根字体为75,如果设计图为640:则rootValue=64)
            propList: ["*"],
            minPixelValue: 2 // 小于2px的不处理，主要解决border: 1px solid
          })
        ]
      }
    }
  },
  // chainWebpack: config => {
  //   config.plugin("preload").tap(() => [
  //     {
  //       rel: "preload",
  //       include: {
  //         type: "all",
  //         entries: ["logo", "login", "register"]
  //       }
  //       // as(entry) {
  //       //   if (/^logo.*\.css$/.test(entry)) return "style";
  //       //   if (/^login.*\.css$/.test(entry)) return "style";
  //       //   if (/^register.*\.css$/.test(entry)) return "style";
  //       //   return "script";
  //       // }
  //     }
  //   ]);
  // },
  pwa: {
    // manifestPath: "public/manifest.json",
    name: "Ritzbiz",
    themeColor: "rgba(25,34,84, 0.3)",
    msTileColor: "#000000",
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "black",
    // assetsVersion: "1.4.0",

    // 配置 workbox 插件
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      // InjectManifest 模式下 swSrc 是必填的。 GenerateSW
      // navigateFallback: "index.html", // GenerateSW
      swSrc: "src/service-worker.js",
      exclude: [/manifest\.json$/],
      importWorkboxFrom: "disabled", // 默认引入 wirkbox 的库 在google cdn ，禁用disabled, local
      importScripts:
        // "https://storage.googleapis.com/workbox-cdn/releases/3.0.0-alpha.3/workbox-sw.js" // 设置自己的worbox 地址
        "https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js" // 设置自己的worbox 地址
      // "https://g.alicdn.com/kg/workbox/4.3.1/workbox-sw.js" // 设置自己的worbox 地址
      // ...其它 Workbox 选项...
    }
  },
  // chainWebpack: config => {
  //   // 解决vue-cli3脚手架创建的项目压缩html 干掉<!-- shell -->导致骨架屏不生效
  //   if (process.env.NODE_ENV !== "development") {
  //     config.plugin("html").tap(opts => {
  //       opts[0].minify.removeComments = false;
  //       return opts;
  //     });
  //   }
  // },
  devServer: {
    proxy: {
      "/manage": {
        // target: 'http://192.168.10.119:8080/pay/',
        // target: 'http://172.16.15.176:8080/pay/',
        // target: 'http://18.140.5.142:8080/pay/',
        // target: 'http://103.75.2.200/pay/',
        // target: 'http://192.168.10.54:8080/pay/',
        target: "http://103.75.2.199:8080/pay",
        // target: 'http://www.zhima2-dev.com/pay/',
        // target: 'http://www.royalbiz.co/pay/',
        // target: "http://192.168.10.54:8090/",
        // target: 'http://52.77.226.129:8080/pay/',
        // target: 'http://192.168.10.12:8085/pay/',
        // target:'http://192.168.10.172:5210/pay/',
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          "^/manage": ""
        },
        onProxyRes: function(proxyRes) {
          // onProxyRes: function(proxyRes, req, res) {
          var cookies = proxyRes.headers["set-cookie"];
          var cookieRegex = /Path=\/pay/i;
          //修改cookie Path
          if (cookies) {
            var newCookie = cookies.map(function(cookie) {
              if (cookieRegex.test(cookie)) {
                return cookie.replace(cookieRegex, "Path=/manage");
              }
              return cookie;
            });
            //修改cookie path
            delete proxyRes.headers["set-cookie"];
            proxyRes.headers["set-cookie"] = newCookie;
          }
        }
      },
      "/wallet": {
        // target: 'http://192.168.10.119:8080/pay/',
        // target: 'http://172.16.15.176:8080/pay/',
        // target: 'http://18.140.5.142:8080/pay/',
        // target: 'http://103.75.2.200/pay/',
        // target: 'http://192.168.10.54:8080/pay/',
        target: "http://103.75.2.199:8080/wallet",
        // target: 'http://www.zhima2-dev.com/pay/',
        // target: 'http://www.royalbiz.co/pay/',
        // target: "http://192.168.10.54:8090/",
        // target: 'http://52.77.226.129:8080/pay/',
        // target: 'http://192.168.10.12:8085/pay/',
        // target:'http://192.168.10.172:5210/pay/',
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          "^/wallet": ""
        },
        onProxyRes: function(proxyRes) {
          // onProxyRes: function(proxyRes, req, res) {
          var cookies = proxyRes.headers["set-cookie"];
          var cookieRegex = /Path=\/pay/i;
          //修改cookie Path
          if (cookies) {
            var newCookie = cookies.map(function(cookie) {
              if (cookieRegex.test(cookie)) {
                return cookie.replace(cookieRegex, "Path=/manage");
              }
              return cookie;
            });
            //修改cookie path
            delete proxyRes.headers["set-cookie"];
            proxyRes.headers["set-cookie"] = newCookie;
          }
        }
      },
      "/websocket": {
        // 11080 20080
        // target: 'http://192.168.10.172:11080',
        // target: 'http://192.168.10.172:11080',
        // target: 'http://192.168.10.172:5210/websocket/',
        // target: 'http://www.zhima2-dev.com/websocket',
        target: "http://103.75.2.199:5210",
        // target: 'http://192.168.10.172:5210/',
        // target: 'http://192.168.10.125:5000/websocket',
        changeOrigin: true,
        secure: false,
        timeout: 1920000, //  for incoming requests
        proxyTimeout: 1920000, // when proxy receives no response from target
        // auth: "username:password",
        pathRewrite: {
          "^/websocket": ""
        }
      },
      // "/websocket/socket.io": {
      //   // 11080 20080
      //   // target: 'http://192.168.10.172:11080',
      //   // target: 'http://192.168.10.172:11080',
      //   // target: 'http://192.168.10.172:5210/websocket/',
      //   // target: 'http://www.zhima2-dev.com/websocket',
      //   target: "http://103.75.2.199:5210/socket.io",
      //   // target: 'http://192.168.10.172:5210/',
      //   // target: 'http://192.168.10.125:5000/websocket',
      //   changeOrigin: true,
      //   // ws: true,// http代理此处不用设置
      //   secure: false, //如果是https接口，需要配置这个参数为true
      //   timeout: 1920000, //  for incoming requests
      //   proxyTimeout: 1920000, // when proxy receives no response from target
      //   // auth: "username:password",
      //   pathRewrite: {
      //     "^/websocket": ""
      //   }
      // }
    }
  }
};
